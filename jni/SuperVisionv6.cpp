#include <jni.h>
#include <stdio.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/video/tracking.hpp>
#include <vector>
#include <android/log.h>
#include <opencv2/highgui/highgui.hpp>
#include <limits>
#include <opencv2/imgproc/imgproc.hpp>

#define  LOG_TAG    "OCV:libnative_activity"
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGW(...)  __android_log_print(ANDROID_LOG_WARN,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

using namespace std;
using namespace cv;

#define MAX_FEATURES 30
int fastThreshold = 20;
//cv::Mat feature_pts1(MAX_FEATURES, 2, CV_32FC1);
//cv::Mat feature_pts2(MAX_FEATURES, 2, CV_32FC1);
vector<KeyPoint> feature_pts1(MAX_FEATURES);
vector<KeyPoint> feature_pts2(MAX_FEATURES);
//cv::Mat err(MAX_FEATURES, 1, CV_32FC1);
vector<float> err;
vector<uchar> status;
//cv::Mat status(MAX_FEATURES, 1, CV_8UC1);
int featureNumber = 0;
jfloat validTrackFeatureNumber = 0;
vector<Point2f> pts1, pts2;
static jfloat motionX = 0;
static jfloat motionY = 0;
CvFont font;
double hScale = 1;
double vScale = 1;
int lineWidth = 2;
char buffer[100] = { 0 };
int errThreshold = 10;
Mat resultMat;
Mat lastMat;
int i =0;

// right, down is positive
void moveMat(Mat& mat, int rowStep, int colStep) {
	int cols = mat.cols;
	int rows = mat.rows;
	if (rowStep >= rows)
		rowStep = rows;
	if (rowStep <= -rows)
		rowStep = -rows;
	if (colStep >= cols)
		colStep = cols;
	if (colStep <= -cols)
		colStep = -cols;

//	Vec4b pix;
	if ((rowStep >= 0) && (colStep >= 0)) {
		for (int i = rows - rowStep - 1; i >= 0; i--)
			for (int j = cols - colStep - 1; j >= 0; j--) {
//				mat.at<uchar>(rowStep + i, colStep + j) = mat.at<uchar>(
				//i, j);
				mat.at<Vec4b>(rowStep + i, colStep + j) = mat.at<Vec4b>(i, j);
			}
		for (int i = 0; i < rowStep; i++)
			for (int j = 0; j < cols; j++) {
				mat.at<Vec4b>(i, j)[0] = 255;
				mat.at<Vec4b>(i, j)[1] = 255;
				mat.at<Vec4b>(i, j)[2] = 255;
				mat.at<Vec4b>(i, j)[3] = 255;
			}
		for (int i = 0; i < rows; i++)
			for (int j = 0; j < colStep; j++) {
				mat.at<Vec4b>(i, j)[0] = 255;
				mat.at<Vec4b>(i, j)[1] = 255;
				mat.at<Vec4b>(i, j)[2] = 255;
				mat.at<Vec4b>(i, j)[3] = 255;
			}
	}
	if ((rowStep >= 0) && (colStep < 0)) {
		for (int i = rows - rowStep - 1; i >= 0; i--)
			for (int j = -colStep; j < cols; j++)
				mat.at<Vec4b>(rowStep + i, j + colStep) = mat.at<Vec4b>(i, j);
		for (int i = 0; i < rowStep; i++) {
			for (int j = 0; j < cols; j++) {
				mat.at<Vec4b>(i, j)[0] = 255;
				mat.at<Vec4b>(i, j)[1] = 255;
				mat.at<Vec4b>(i, j)[2] = 255;
				mat.at<Vec4b>(i, j)[3] = 255;
			}
		}
		for (int i = 0; i < rows; i++) {
			for (int j = cols + colStep; j < cols; j++) {
				mat.at<Vec4b>(i, j)[0] = 255;
				mat.at<Vec4b>(i, j)[1] = 255;
				mat.at<Vec4b>(i, j)[2] = 255;
				mat.at<Vec4b>(i, j)[3] = 255;
			}
		}
	}

	if ((rowStep < 0) && (colStep >= 0)) {
		for (int i = -rowStep; i < rows; i++)
			for (int j = cols - colStep - 1; j >= 0; j--)
				mat.at<Vec4b>(i + rowStep, colStep + j) = mat.at<Vec4b>(i, j);
		for (int i = rows + rowStep; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				mat.at<Vec4b>(i, j)[0] = 255;
				mat.at<Vec4b>(i, j)[1] = 255;
				mat.at<Vec4b>(i, j)[2] = 255;
				mat.at<Vec4b>(i, j)[3] = 255;
			}
		}
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < colStep; j++) {
				mat.at<Vec4b>(i, j)[0] = 255;
				mat.at<Vec4b>(i, j)[1] = 255;
				mat.at<Vec4b>(i, j)[2] = 255;
				mat.at<Vec4b>(i, j)[3] = 255;
			}
		}
	}

	if ((rowStep < 0) && (colStep < 0)) {
		for (int i = -rowStep; i < rows; i++)
			for (int j = -colStep; j < cols; j++)
				mat.at<Vec4b>(i + rowStep, j + colStep) = mat.at<Vec4b>(i, j);
		for (int i = rows + rowStep; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				mat.at<Vec4b>(i, j)[0] = 255;
				mat.at<Vec4b>(i, j)[1] = 255;
				mat.at<Vec4b>(i, j)[2] = 255;
				mat.at<Vec4b>(i, j)[3] = 255;
			}
		}
		for (int i = 0; i < rows; i++) {
			for (int j = cols + colStep; j < cols; j++) {
				mat.at<Vec4b>(i, j)[0] = 255;
				mat.at<Vec4b>(i, j)[1] = 255;
				mat.at<Vec4b>(i, j)[2] = 255;
				mat.at<Vec4b>(i, j)[3] = 255;
			}
		}
	}
}

/*! @brief shift the values in a matrix by an (x,y) offset
 *
 * Given a matrix and an integer (x,y) offset, the matrix will be shifted
 * such that:
 *
 * 	src(a,b) ---> dst(a+y,b+x)
 *
 * In the case of a non-integer offset, (e.g. cv::Point2f(-2.1, 3.7)),
 * the shift will be calculated with subpixel precision using bilinear
 * interpolation.
 *
 * All valid OpenCV datatypes are supported. If the source datatype is
 * fixed-point, and a non-integer offset is supplied, the output will
 * be a floating point matrix to preserve subpixel accuracy.
 *
 * All border types are supported. If no border type is supplied, the
 * function defaults to BORDER_CONSTANT with 0-padding.
 *
 * The function supports in-place operation.
 *
 * Some common examples are provided following:
 * \code
 * 	// read an image from file
 * 	Mat mat = imread(filename);
 * 	Mat dst;
 *
 * 	// Perform Matlab-esque 'circshift' in-place
 * 	shift(mat, mat, Point(5, 5), BORDER_WRAP);
 *
 * 	// Perform shift with subpixel accuracy, padding the missing pixels with 1s
 * 	// NOTE: if mat is of type CV_8U, then it will be converted to type CV_32F
 * 	shift(mat, mat, Point2f(-13.7, 3.28), BORDER_CONSTANT, 1);
 *
 * 	// Perform subpixel shift, preserving the boundary values
 * 	shift(mat, dst, Point2f(0.093, 0.125), BORDER_REPLICATE);
 *
 * 	// Perform a vanilla shift, integer offset, very fast
 * 	shift(mat, dst, Point(2, 2));
 * \endcode
 *
 * @param src the source matrix
 * @param dst the destination matrix, can be the same as source
 * @param delta the amount to shift the matrix in (x,y) coordinates. Can be
 * integer of floating point precision
 * @param fill the method used to fill the null entries, defaults to BORDER_CONSTANT
 * @param value the value of the null entries if the fill type is BORDER_CONSTANT
 */
void shift(const cv::Mat& src, cv::Mat& dst, cv::Point2f delta, int fill =
		cv::BORDER_CONSTANT, cv::Scalar value = cv::Scalar(0, 0, 0, 0)) {

	// error checking
	assert(fabs(delta.x) < src.cols && fabs(delta.y) < src.rows);

	// split the shift into integer and subpixel components
	cv::Point2i deltai(ceil(delta.x), ceil(delta.y));
	cv::Point2f deltasub(fabs(delta.x - deltai.x), fabs(delta.y - deltai.y));

	// INTEGER SHIFT
	// first create a border around the parts of the Mat that will be exposed
	int t = 0, b = 0, l = 0, r = 0;
	if (deltai.x > 0)
		l = deltai.x;
	if (deltai.x < 0)
		r = -deltai.x;
	if (deltai.y > 0)
		t = deltai.y;
	if (deltai.y < 0)
		b = -deltai.y;
	cv::Mat padded;
	cv::copyMakeBorder(src, padded, t, b, l, r, fill, value);

	// SUBPIXEL SHIFT
	float eps = std::numeric_limits<float>::epsilon();
	if (deltasub.x > eps || deltasub.y > eps) {
		switch (src.depth()) {
		case CV_32F: {
			cv::Matx<float, 1, 2> dx(1 - deltasub.x, deltasub.x);
			cv::Matx<float, 2, 1> dy(1 - deltasub.y, deltasub.y);
			sepFilter2D(padded, padded, -1, dx, dy, cv::Point(0, 0), 0,
					cv::BORDER_CONSTANT);
			break;
		}
		case CV_64F: {
			cv::Matx<double, 1, 2> dx(1 - deltasub.x, deltasub.x);
			cv::Matx<double, 2, 1> dy(1 - deltasub.y, deltasub.y);
			sepFilter2D(padded, padded, -1, dx, dy, cv::Point(0, 0), 0,
					cv::BORDER_CONSTANT);
			break;
		}
		default: {
			cv::Matx<float, 1, 2> dx(1 - deltasub.x, deltasub.x);
			cv::Matx<float, 2, 1> dy(1 - deltasub.y, deltasub.y);
			padded.convertTo(padded, CV_32F);
			sepFilter2D(padded, padded, CV_32F, dx, dy, cv::Point(0, 0), 0,
					cv::BORDER_CONSTANT);
			break;
		}
		}
	}

	// construct the region of interest around the new matrix
	cv::Rect roi = cv::Rect(std::max(-deltai.x, 0), std::max(-deltai.y, 0), 0,
			0) + src.size();
	dst = padded(roi);
}

extern "C" {
JNIEXPORT void JNICALL Java_edu_harvard_schepens_supervisionv7_CameraPreview_simpleShow(
		JNIEnv* env, jobject obj, jlong addrColorMat, jint cvWindowSize);

JNIEXPORT void JNICALL Java_edu_harvard_schepens_supervisionv7_CameraPreview_FindFeatures(
		JNIEnv*, jobject, jlong addrGray, jlong addrRgba);

JNIEXPORT jfloat JNICALL Java_edu_harvard_schepens_supervisionv7_CameraPreview_MotionEstimation(
		JNIEnv*, jobject, jlong addrGray, jlong addrRgba, jlong addrColorMat,
		jint cvWindorSize);

JNIEXPORT void JNICALL Java_edu_harvard_schepens_supervisionv7_CameraPreview_resetMotion(
		JNIEnv*, jobject);

JNIEXPORT void JNICALL Java_edu_harvard_schepens_supervisionv7_CameraPreview_putFPS(JNIEnv*,
		jobject, jlong addrRgba, jint fps);

JNIEXPORT void JNICALL Java_edu_harvard_schepens_supervisionv7_CameraPreview_RGB2GREY(JNIEnv*,
		jobject, jlong addrRgba);

JNIEXPORT jfloatArray JNICALL Java_edu_harvard_schepens_supervisionv7_CameraPreview_MotionEstimation2(
		JNIEnv* env, jobject obj, jlong addrLastMat, jlong addrCurrentMat);

JNIEXPORT void JNICALL Java_edu_harvard_schepens_supervisionv7_CameraPreview_putFPS(JNIEnv*,
		jobject obj, jlong addrRgba, jint fps) {
	cvInitFont(&font, CV_FONT_HERSHEY_SIMPLEX, hScale, vScale, 0, lineWidth);
	Mat& rgbMat = *(Mat*) addrRgba;
	sprintf(buffer, "FPS:%d", fps);
	string message = buffer;
	putText(rgbMat, message, cvPoint(50, 50), 2, 1, Scalar(0, 255, 0, 255));
	return;
}

JNIEXPORT void JNICALL Java_edu_harvard_schepens_supervisionv7_CameraPreview_RGB2GREY(JNIEnv*,
		jobject obj, jlong addrRgba) {

	Mat& rgbMat = *(Mat*) addrRgba;
	int cols = rgbMat.cols;
	int rows = rgbMat.rows;

	int grey = 0;
	/*for(int i = 0; i < cols; i++){
		for(int j = 0; j < rows; j++){
			grey = int(0.299f*rgbMat.at<Vec4b>(j,i)[0]+0.587f*rgbMat.at<Vec4b>(j,i)[1]+0.115f*rgbMat.at<Vec4b>(j,i)[1]);
			rgbMat.at<Vec4b>(j,i)[0] = grey;
			rgbMat.at<Vec4b>(j,i)[1] = grey;
			rgbMat.at<Vec4b>(j,i)[2] = grey;

			//rgbMat.at<Vec4b>(j,i)[3] = grey;
			//LOGD("Grey:%d",grey);
		}
	}*/
	rgbMat = rgbMat.dot(cv::Matx14f(0.299,0.587,0.115));
	//rgbMat = rgbMat.dot(cv::Matx14f(0.115,0.587,0.299));
	return;
}

JNIEXPORT void JNICALL Java_edu_harvard_schepens_supervisionv7_CameraPreview_FindFeatures(
		JNIEnv*, jobject, jlong addrGray, jlong addrRgba) {
	Mat& mGr = *(Mat*) addrGray;
	Mat& mRgb = *(Mat*) addrRgba;
	vector<KeyPoint> v;
	FastFeatureDetector detector(50);
	detector.detect(mGr, v);
	/* draw feature points on screen.
	for (int i = 0; i < v.size(); i++) {
		const KeyPoint& kp = v[i];
		circle(mRgb, Point(kp.pt.x, kp.pt.y), 10, Scalar(255, 0, 0, 255));
	}
	*/
}

JNIEXPORT void JNICALL Java_edu_harvard_schepens_supervisionv7_CameraPreview_resetMotion(
		JNIEnv*, jobject) {
	motionX = 0;
	motionY = 0;
}

JNIEXPORT void JNICALL Java_edu_harvard_schepens_supervisionv7_CameraPreview_simpleShow(
		JNIEnv* env, jobject obj, jlong addrColorMat, jint cvWindowSize) {
	Mat& rgbMat = *(Mat*) addrColorMat;
	int screenHeight = rgbMat.rows;
	int screenWidth = rgbMat.cols;
	int startRow = (screenHeight - cvWindowSize) / 2;
	int endRow = (screenHeight + cvWindowSize) / 2;
	int startCol = (screenWidth - cvWindowSize) / 2;
	int endCol = (screenWidth + cvWindowSize) / 2;
	Range rowRange = Range(startRow, endRow);
	Range colRange = Range(startCol, endCol);
	resultMat = rgbMat(rowRange, colRange);
	resize(resultMat, resultMat, rgbMat.size());
	rgbMat = resultMat;
}

JNIEXPORT jfloatArray JNICALL Java_edu_harvard_schepens_supervisionv7_CameraPreview_MotionEstimation2(
		JNIEnv* env, jobject obj, jlong addrLastMat, jlong addrCurrentMat) {
	Mat& lastMat = *(Mat*) addrLastMat;
	Mat& currentMat = *(Mat*) addrCurrentMat;
	FastFeatureDetector detector(fastThreshold);
	detector.detect(lastMat, feature_pts1);
	featureNumber = feature_pts1.size();
	KeyPoint::convert(feature_pts1, pts1);

	if (featureNumber == 0) {

	}


	if (featureNumber != 0) {
		calcOpticalFlowPyrLK(lastMat, currentMat, pts1, pts2, status, err);
		//  clear valid feature number first
		jfloat diffX = 0;
		jfloat diffY = 0;
		jfloat avgX = 0;
		jfloat avgY = 0;
		validTrackFeatureNumber = 0;

		if (featureNumber >= MAX_FEATURES) {
			featureNumber = MAX_FEATURES;
		}
		//  count the number of valid tracking features
		for (int i = 0; i < featureNumber; i++) {
			if ((status[i] == 1) && (err[i] <= errThreshold)) {
				validTrackFeatureNumber++;
				Point2f &pt1 = pts1[i];
				Point2f &pt2 = pts2[i];
				diffX += pt2.x - pt1.x;
				diffY += pt2.y - pt1.y;

			}
		}

		//  if there are available tracking features, get the average
		if (validTrackFeatureNumber > 0) {
			avgX = diffX / validTrackFeatureNumber;
			avgY = diffY / validTrackFeatureNumber;
		}
//		motionX += avgX;
//		motionY += avgY;
		motionX = avgX;
		motionY = avgY;

	}
	jfloatArray result;
	result = env->NewFloatArray(2);
	if (result == NULL) {
	    return NULL; /* out of memory error thrown */
	}

	jfloat array[2];
	array[0] = motionX;
	array[1] = motionY;
	env->SetFloatArrayRegion(result, 0, 2, array);
	return result;
}


JNIEXPORT jfloat JNICALL Java_edu_harvard_schepens_supervisionv7_CameraPreview_MotionEstimation(
		JNIEnv* env, jobject obj, jlong addrLastMat, jlong addrCurrentMat,
		jlong addrColorMat, jint cvWindowSize) {
	Mat& lastMat = *(Mat*) addrLastMat;
	Mat& currentMat = *(Mat*) addrCurrentMat;
	Mat& rgbMat = *(Mat*) addrColorMat;

	// 1080 rows				1920 cloums
	clock_t start_time = clock();

	FastFeatureDetector detector(fastThreshold);
	detector.detect(lastMat, feature_pts1);

	clock_t end_time = clock();
	double ellapse = (end_time - start_time) * 1000.0 / CLOCKS_PER_SEC;
//	LOGE("detection time is:%f\n", ellapse);

	start_time = clock();

	featureNumber = feature_pts1.size();
	KeyPoint::convert(feature_pts1, pts1);

	if (featureNumber != 0) {

	calcOpticalFlowPyrLK(lastMat, currentMat, pts1, pts2, status, err);

	end_time = clock();
	ellapse = (end_time - start_time) * 1000.0 / CLOCKS_PER_SEC;
//	LOGE("tracking time is:%f\n", ellapse);

	//  clear valid feature number first
	jfloat diffX = 0;
	jfloat diffY = 0;
	jfloat avgX = 0;
	jfloat avgY = 0;
	validTrackFeatureNumber = 0;
	if (featureNumber >= MAX_FEATURES) {
		featureNumber = MAX_FEATURES;
	}
	//  count the number of valid tracking features
	for (int i = 0; i < featureNumber; i++) {
		if ((status[i] == 1) && (err[i] <= errThreshold)) {
			validTrackFeatureNumber++;
			Point2f &pt1 = pts1[i];
			Point2f &pt2 = pts2[i];
			diffX += pt2.x - pt1.x;
			diffY += pt2.y - pt1.y;
			/*
			circle(rgbMat, Point(pt1.x + 860, pt1.y + 440), 10,
					Scalar(0, 255, 0, 255));
			circle(rgbMat, Point(pt2.x + 860, pt2.y + 440), 10,
					Scalar(255, 0, 0, 255));
			line(rgbMat, Point(pt1.x + 860, pt1.y + 440),
					Point(pt2.x + 860, pt2.y + 440), Scalar(0, 255, 0, 255));
			*/

		}
	}

	//  if there are available tracking features, get the average
	if (validTrackFeatureNumber > 0) {
		avgX = diffX / validTrackFeatureNumber;
		avgY = diffY / validTrackFeatureNumber;
	}
	motionX += avgX;
	motionY += avgY;
//	cvInitFont(&font, CV_FONT_HERSHEY_SIMPLEX, hScale, vScale, 0, lineWidth);
//	sprintf(buffer, "x:%d, y:%d", motionX, motionY);
//	string message = buffer;
//	putText(rgbMat, message, cvPoint(200, 200), 2, 1,  Scalar(0,255,0,255));
//    LOGI("motion x and motion y is: %d,%d\n", motionX, motionY);
	start_time = clock();
	ellapse = (start_time - end_time) * 1000.0 / CLOCKS_PER_SEC;
//    LOGE("post process time is:%f", ellapse);

	//moveMat(rgbMat, -motionY, -motionX);
//    shift(rgbMat, rgbMat, Point(-motionX, -motionY));
	}

	int screenHeight = rgbMat.rows;
	int screenWidth = rgbMat.cols;
	int startRow = (screenHeight - cvWindowSize) / 2 + motionY;
	int endRow = (screenHeight + cvWindowSize) / 2 + motionY;
	int startCol = (screenWidth - cvWindowSize) / 2 + motionX;
	int endCol = (screenWidth + cvWindowSize) / 2 + motionX;
	Range rowRange = Range(startRow, endRow);
	Range colRange = Range(startCol, endCol);
	resultMat = rgbMat(rowRange, colRange);
	resize(resultMat, resultMat, rgbMat.size());
	rgbMat = resultMat;




	end_time = clock();
	ellapse = (end_time - start_time) * 1000.0 / CLOCKS_PER_SEC;
//	LOGE("shift time is:%f\n", ellapse);

	return validTrackFeatureNumber;
}
}
