package edu.harvard.schepens.supervisionv7;

import java.util.ArrayList;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.PixelFormat;
import android.hardware.Camera.Size;

public class Setting {
	
	/**
	 * The basic scaling factor of the CameraPreview.
	 */
	
	public static float BASIC_SCALE_FIXED = 2;
	
	/**
	 * The basic scaling factor of the CameraPreview after view field fixed is considered.
	 */
	public static float BASIC_SCALE = 2;
	
	/**
	 * The view field should be fixed if true.
	 */
	public static final boolean VIEW_FIELD_FIXED = true; 

	/**
	 * Decides the area that triggers browsing gesture.
	 */
	public static final float BROWSING_GESTURE_START_THRESHOLD = 1 / 10f;

	/**
	 * Decides the distance of a successful browsing gesture.
	 */
	public static final float BROWSING_GESTURE_END_THRESHOLD = 1 / 4f;

	/**
	 * The maximum scaling factor of the CameraPreview. For example, if the
	 * basis_scale is 3, and the max_scale_factor is 4, then the CameraPreview
	 * can be scaled to 3x4=12 times at most.
	 */
	public static final float MAX_SCALE_FACTOR = 4;
	/**
	 * The max zoom value of the camera. 0 means no zooming, 10 means 2 times
	 * magnification, 20 means 3 times magnification and so on. <br>
	 * Camera.getParameters.getZoomRatios() does not always give correct ratio
	 * (at least on Galaxy S3 mini and S4 mini it doesn't).
	 */
	public static float MAX_ZOOMVALUE = 40;
	/**
	 * true then the preview size is automatically selected.
	 */
	public static boolean preview_auto = true;
	/**
	 * Current preview height.
	 */
	public static int preview_height = 0;
	/**
	 * Current preview width.
	 */
	public static int preview_width = 0;
	/**
	 * Max preview width. No preview with width larger than this value will be
	 * shown and selected on the menu.
	 */
	public static final int MAX_FRAME_WIDTH = 1920;
	/**
	 * Max preview height. No preview with height larger than this value will be
	 * shown and selected on the menu.
	 */
	public static final int MAX_FRAME_HEIGHT = 1080;
	
	public static final int MIN_FRAME_WIDTH = 640;
	
	public static final int MIN_FRAME_HEIGHT = 480;
	
	/**
	 * List of all supported preview sizes that are no larger than the
	 * {@link #MAX_FRAME_WIDTH} and {@link #MAX_FRAME_HEIGHT}.
	 */
	public static ArrayList<Size> supportedPreviewSizes;
	/**
	 * The basic dimension of the focus area.
	 */
	public static final int FOCUS_WIDTH = 400;

	/**
	 * True then allows the preview and scrolling of locked screen go out of
	 * boundaries.
	 */
	public static final boolean ALLOW_OUT_OF_BOUNDARY = false;

	/**
	 * True then allows the info page to reflow after zooming in or out.
	 */
	public static final boolean ALLOW_INFO_REFLOW = false;

	/**
	 * The subject code user enters must start with this prefix. <b>
	 * 
	 * @see {@link #invalidateSubjectId(String)}
	 */
	public static final String SUBJECT_CODE = "s";
	/**
	 * The group code user enters must start with this prefix.
	 * 
	 * @see {@link #validateGroup(String)}
	 */
	public static final String GROUP_CODE = "g";

	/**
	 * If a touch event is shorter than this interval (in ms), then an auto
	 * focus event is operated.
	 */
	public static final long AUTO_FOCUS_INTERVAL = 500;

	public static final long STABILIZATION_THRESHOLD = 1000;

	/**
	 * Maximum length of the inputing subject id.
	 * 
	 * @see {@link #invalidateSubjectId(String)}
	 */
	public static final int SUBJECT_ID_MAX_LENGTH = 4;
	/**
	 * Minimum length of the inputing subject id.
	 * 
	 * @see {@link #invalidateSubjectId(String)}
	 */
	public static final int SUBJECT_ID_MIN_LENGTH = 1;
	/**
	 * True if the user is a subject.
	 */
	public static boolean isSubject = false;

	/**
	 * Subject Id of the user if he is a subject.
	 */
	public static String subjectId = null;
	/**
	 * True if the user is in the group.
	 */
	public static boolean isInGroup = false;
	/**
	 * Group id of the user if he is in a group.
	 */
	public static String groupId = null;

	/**
	 * Key for storing and getting the preference of {@link #isSubject}
	 */
	public static final String PREFERENCE_IS_SUBJECT = "isSubject";
	/**
	 * Key for storing and getting the preference of {@link #isInGroup};
	 */
	public static final String PREFERENCE_IS_IN_GROUP = "isInGroup";
	/**
	 * Key for storing and getting the preference of {@link #subjectId};
	 */
	public static final String PREFERENCE_SUBJECT_ID = "SubjectId";
	/**
	 * Key for storing and getting the preference of {@link #groupId};
	 */
	public static final String PREFERENCE_GROUP_ID = "GroupId";
	/**
	 * Key for storing and getting the preference of {@link #preview_auto};
	 */
	public static final String PREFERENCE_PREVIEW_IS_AUTO = "preview_auto";
	/**
	 * Key for storing and getting the preference of {@link #preview_width};
	 */
	public static final String PREFERENCE_PREVIEW_WIDTH = "preview_width";
	/**
	 * Key for storing and getting the preference of {@link #preview_height}
	 */
	public static final String PREFERENCE_PREVIEW_HEIGHT = "preview_height";

	public static final String PREFERENCE_SHOULD_SEND_FOCUS = "should_send_focus";
	
	public static final String PREFERENCE_INSTALLED_DATE = "installed date";
	
	public static final String PREFERENCE_LAST_ACTIVE_DATE = "last active date";
	
	public static final String PREFERENCE_TOTAL_ACTIVE_DAYS = "total active days";

	public static boolean shouldSendFocus = false;

	/**
	 * Used for filling up the subject id if it is less than 4 digits.
	 */
	public static final String[] CARRY_STRING = { "", "0", "00", "000" };

	/**
	 * For test purpose of Samsumg Galaxy SIII taking picture bug.
	 */
	public static final boolean testForTakingPicture = false;

	/**
	 * Enable the FpsMeter if true.
	 */
	public static final boolean enableFpsMeter = false;

	/**
	 * Set the pixel format of surfaceview.
	 */
	// public static final int PIXEL_FORMAT = PixelFormat.RGBA_8888;
	public static final int PIXEL_FORMAT = PixelFormat.RGB_565;
	/**
	 * Set the pixel format of cached bitmap.
	 */
	public static final Config BITMAP_FORMAT = Bitmap.Config.RGB_565;
	// public static final int PIXEL_FORMAT = PixelFormat.OPAQUE;
	/**
	 * Optimization by using a YUV2RGB instead of YUV2RGBA conversion, and
	 * setting the pixel format for surfaceview, and bitmap configuration for
	 * CachedBitmap.
	 * <p>
	 * It is said that if the bitmap pixel format is the same as the surfaceview
	 * pixel format, then the drawBitmap would be much faster. But I doubt that.
	 */
	public static final boolean doPixelFormatOptimization = false;

	/**
	 * Using android built in color filter. This is slow.
	 */
	public static final int FILTER_NATIVE = 0;
	/**
	 * Using OpenCV method to do the color filter. Fastest method.
	 */
	public static final int FILTER_OPENCV = 1;
	/**
	 * Although Google claims that the renderscript is fast, it is 5 times
	 * slower than OpenCV method, even when running on the newest android
	 * version (on lower version it does not have GPU support.)
	 */
	public static final int FILTER_RENDERSCRIPT = 2;
	/**
	 * This is the fastest way to convert YUV to RGB, but cannot handle other
	 * color matrix.
	 */
	public static final int FILTER_CVTCOLOR = 3;
	/**
	 * It is said that intrinsic renderscript is much faster than the hand
	 * written, it is still no bettern than the Native one.
	 */
	public static final int FILTER_INTRINSIC_RENDERSCRIPT = 4;
	/**
	 * Current filter method. Possible values:{@link #FILTER_CVTCOLOR},
	 * {@link #FILTER_INTRINSIC_RENDERSCRIPT}, {@link #FILTER_NATIVE},
	 * {@link #FILTER_OPENCV}, {@link #FILTER_RENDERSCRIPT};
	 */
	public static int filterMethod = FILTER_OPENCV;

	/**
	 * No enhancement is made.
	 */
	public static final int ENHANCING_NORMAL = 0;
	/**
	 * Convert RGB to Greyscale.
	 */
	public static final int ENHANCING_RGB2GRAY = 1;
	/**
	 * Current enhancing mode. Possible values: {@link #ENHANCING_NORMAL},
	 * {@link #ENHANCING_RGB2GRAY};
	 */
	public static int enhancingMode = ENHANCING_NORMAL;

	/**
	 * True then before taking picture, an auto focus will be operated.
	 * <p>
	 * Not used because on some devices, it takes too long to do an auto focus.
	 */
	public static final boolean focusBeforeTakePicture = false;

	public static final String DESCRIPTOR = "SuperVision+";
	public static final String TARGET_URL_GOOGLE_PLAY = "https://play.google.com/store/apps/details?id=edu.harvard.schepens.supervisionv7";

public static final boolean smoothZooming = true;
	
	/**
	 * Margins between every button on the screen.
	 */
	public static final int BUTTON_MARGIN = 20;

	/**
	 * Size of every button on the screen.
	 */
	public static final int BUTTON_SIZE = 60;
	
	public static final String APPKEY_360 = "1f032011c8a1d8c1fa83a6f4558b01a0";
	
	/**
	 * True then display the toast information for debugging.
	 */
	public static final boolean displayToastForDebugging = false;

	/**
	 * Given a subject code, checks if this subject id has the prefix
	 * {@link #SUBJECT_CODE} and also satisfies {@link #SUBJECT_ID_MAX_LENGTH}
	 * and {@link #SUBJECT_ID_MIN_LENGTH}. <br>
	 * If it is a valid subject id, then update {@link #subjectId} and
	 * {@link #isSubject}.
	 * 
	 * @param code
	 */
	public static void invalidateSubjectId(String code) {
		clearAllAuthorization();
		if (code.length() < Setting.SUBJECT_CODE.length()) {
			return;
		}
		String authorization = code.substring(0, SUBJECT_CODE.length());
		if (authorization.equals(SUBJECT_CODE)) {

			subjectId = code.substring(SUBJECT_CODE.length());

			if (subjectId.length() > SUBJECT_ID_MAX_LENGTH
					|| subjectId.length() < SUBJECT_ID_MIN_LENGTH) {
				isSubject = false;
				subjectId = null;
			} else {
				try {
					Integer.parseInt(subjectId);
					isSubject = true;
					int length = subjectId.length();
					subjectId = CARRY_STRING[4 - length] + subjectId;
				} catch (Exception e) {
					isSubject = false;
				}

			}
		}
	}

	/**
	 * Clear all subject and group information.
	 */
	public static void clearAllAuthorization() {
		isInGroup = false;
		isSubject = false;
	}

	/**
	 * Clear group information.
	 */
	public static void clearAllGroup() {
		isInGroup = false;

	}

	/**
	 * Given a group code, checks if it has the prefix {@link #GROUP_CODE}. <br>
	 * If it is a valid group code, updates {@link #groupId} and
	 * {@link #isInGroup};
	 * 
	 * @param code
	 */
	public static void validateGroup(String code) {
		clearAllGroup();
		if (code.length() <= GROUP_CODE.length()) {
			return;
		}
		String c = code.substring(0, GROUP_CODE.length());
		if (c.equals(Setting.GROUP_CODE)) {
			groupId = code.substring(GROUP_CODE.length());
			isInGroup = true;
		}

	}

}
