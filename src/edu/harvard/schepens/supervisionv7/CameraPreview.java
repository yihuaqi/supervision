package edu.harvard.schepens.supervisionv7;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.Area;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;

public class CameraPreview extends MyCameraBridgeViewBase implements
		PreviewCallback {

	private static final int MAGIC_TEXTURE_ID = 10;
	private static final String TAG = "JavaCameraView";
	private static final String TAG_PreviewSize = "Preview Size";
	private byte mBuffer[];
	private Mat[] mFrameChain;
	private int mChainIdx = 0;
	private Thread mThread;
	private boolean mStopThread;
	protected Camera mCamera;
	protected JavaCameraFrame[] mCameraFrame;
	private SurfaceTexture mSurfaceTexture;

	/**
	 * Previous Frame mat that is going to be compared with the current one.
	 */
	private Mat prevMat;

	/**
	 * It should do stabilization if it is true.
	 */
	protected boolean shouldEstablize = false;

	/**
	 * True if the image is horizontally locked.
	 */
	protected boolean isHorizontalLocked = true;

	/**
	 * True if the flash is turned on.
	 */
	private boolean isFlashOn = false;

	/**
	 * Checks if auto focus is supported.
	 */
	protected boolean supportFocus = false;

	/**
	 * If true then the CameraPreview will freeze the frame data.
	 */
	private boolean isFrozen = false;

	/**
	 * The Mat that holds the last frame data before frozen.
	 */
	private Mat frozonMat = null;

	private Bitmap frozonBitmap = null;

	private int mScreenWidth = 0;
	private int mScreenHeight = 0;

	/**
	 * The orientation mode of the phone. It is used for doing Horizontal lock.
	 */
	private int mOrientationMode = 0;

	public static class JavaCameraSizeAccessor implements ListItemAccessor {

		public int getWidth(Object obj) {
			Camera.Size size = (Camera.Size) obj;
			return size.width;
		}

		public int getHeight(Object obj) {
			Camera.Size size = (Camera.Size) obj;
			return size.height;
		}
	}

	public CameraPreview(Context context, int cameraId) {
		super(context, cameraId);
	}

	public CameraPreview(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	/**
	 * Initialize the camera.
	 * 
	 * @param width
	 *            width of the CameraPreview.
	 * @param height
	 *            height of the CameraPreview.
	 * @return true if it is successfully initialized.
	 */
	protected boolean initializeCamera(int width, int height) {

		mScreenHeight = height;
		mScreenWidth = width;

		boolean result = true;
		synchronized (this) {
			mCamera = null;

			if (mCameraIndex == CAMERA_ID_ANY) {

				try {
					mCamera = Camera.open();
				} catch (Exception e) {

				}

				if (mCamera == null
						&& Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
					boolean connected = false;
					for (int camIdx = 0; camIdx < Camera.getNumberOfCameras(); ++camIdx) {

						try {
							mCamera = Camera.open(camIdx);
							connected = true;
						} catch (RuntimeException e) {

						}
						if (connected)
							break;
					}
				}
			} else {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
					int localCameraIndex = mCameraIndex;
					if (mCameraIndex == CAMERA_ID_BACK) {

						Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
						for (int camIdx = 0; camIdx < Camera
								.getNumberOfCameras(); ++camIdx) {
							Camera.getCameraInfo(camIdx, cameraInfo);
							if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
								localCameraIndex = camIdx;
								break;
							}
						}
					} else if (mCameraIndex == CAMERA_ID_FRONT) {

						Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
						for (int camIdx = 0; camIdx < Camera
								.getNumberOfCameras(); ++camIdx) {
							Camera.getCameraInfo(camIdx, cameraInfo);
							if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
								localCameraIndex = camIdx;
								break;
							}
						}
					}
					if (localCameraIndex == CAMERA_ID_BACK) {

					} else if (localCameraIndex == CAMERA_ID_FRONT) {

					} else {

						try {
							mCamera = Camera.open(localCameraIndex);
						} catch (RuntimeException e) {

						}
					}
				}
			}

			if (mCamera == null)
				return false;

			/* Now set camera parameters */
			try {
				Camera.Parameters params = mCamera.getParameters();

				Setting.MAX_ZOOMVALUE = params.getMaxZoom();
				if (params.getMaxNumFocusAreas() > 0
						&& params.getMaxNumFocusAreas() > 0) {
					supportFocus = true;
				}

				params.setFocusMode(Parameters.FOCUS_MODE_AUTO);

				List<android.hardware.Camera.Size> sizes = params
						.getSupportedPreviewSizes();

				if (sizes != null) {
					/*
					 * Select the size that fits surface considering maximum
					 * size allowed
					 */
					Size frameSize = null;
					if (Setting.preview_auto) {
						frameSize = calculateCameraFrameSize(sizes,
								new JavaCameraSizeAccessor(), width, height);
					} else {
						frameSize = new Size(Setting.preview_width,
								Setting.preview_height);
					}
					params.setPreviewFormat(ImageFormat.NV21);

					params.setPreviewSize((int) frameSize.width,
							(int) frameSize.height);
// This will cause the camera to lose focus right before taking picture (taking picture without auto focus first)
//					List<String> FocusModes = params.getSupportedFocusModes();
//					if (FocusModes != null
//							&& FocusModes
//									.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO)) {
//						params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
//					}

					mCamera.setParameters(params);

					params = mCamera.getParameters();

					mFrameWidth = params.getPreviewSize().width;
					mFrameHeight = params.getPreviewSize().height;
					
					if(Setting.VIEW_FIELD_FIXED){
						Log.d("ScreenSize",mScreenHeight+" "+mScreenWidth+" "+mFrameHeight+" "+mFrameWidth);
						
						Setting.BASIC_SCALE = Setting.BASIC_SCALE_FIXED*(float)Math.min(mScreenHeight, mScreenWidth)/(float)Math.min(mFrameHeight, mFrameWidth);
						Log.d("ScreenSize","BASIC_SCALE="+Setting.BASIC_SCALE);
					}
					
					mScale = Setting.BASIC_SCALE;

					if (mFpsMeter != null) {
						mFpsMeter.setResolution(mFrameWidth, mFrameHeight);
					}

					int size = mFrameWidth * mFrameHeight;
					size = size
							* ImageFormat.getBitsPerPixel(params
									.getPreviewFormat()) / 8;
					mBuffer = new byte[size];

					mCamera.addCallbackBuffer(mBuffer);
					mCamera.setPreviewCallbackWithBuffer(this);

					mFrameChain = new Mat[2];
					mFrameChain[0] = new Mat(mFrameHeight + (mFrameHeight / 2),
							mFrameWidth, CvType.CV_8UC1);
					mFrameChain[1] = new Mat(mFrameHeight + (mFrameHeight / 2),
							mFrameWidth, CvType.CV_8UC1);

					AllocateCache();

					// mCameraFrame = new
					// JavaCameraFrame(mFrameChain[mChainIdx], mFrameWidth,
					// mFrameHeight,mFrameChain);

					mCameraFrame = new JavaCameraFrame[2];
					mCameraFrame[0] = new JavaCameraFrame(mFrameChain[0],
							mFrameWidth, mFrameHeight);
					mCameraFrame[1] = new JavaCameraFrame(mFrameChain[1],
							mFrameWidth, mFrameHeight);

					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
						mSurfaceTexture = new SurfaceTexture(MAGIC_TEXTURE_ID);
						mCamera.setPreviewTexture(mSurfaceTexture);
					} else
						mCamera.setPreviewDisplay(null);

					/* Finally we are ready to start the preview */

					mCamera.startPreview();

				} else
					result = false;
			} catch (Exception e) {
				result = false;
				e.printStackTrace();
			}
		}
		calculatePreviewSize();
		return result;
	}

	/**
	 * Calculate the supported preview size, and added to
	 * Setting.supportedPreviewSizes;
	 */
	protected void calculatePreviewSize() {
		if (Setting.supportedPreviewSizes == null) {
			Parameters params = mCamera.getParameters();
			List<Camera.Size> allSizes = params.getSupportedPreviewSizes();
			Setting.supportedPreviewSizes = new ArrayList<Camera.Size>();
			for (int i = 0; i < allSizes.size(); i++) {
				Camera.Size temp = allSizes.get(i);
				Log.d("Preview Size", temp.height+" "+temp.width);
				if (temp.height <= Setting.MAX_FRAME_HEIGHT && temp.height >= Setting.MIN_FRAME_HEIGHT
						&& temp.width <= Setting.MAX_FRAME_WIDTH && temp.width >= Setting.MIN_FRAME_WIDTH) {
					Setting.supportedPreviewSizes.add(temp);
				}
			}
		}
	}

	protected void releaseCamera() {
		synchronized (this) {
			if (mCamera != null) {
				mCamera.stopPreview();
				mCamera.setPreviewCallback(null);

				mCamera.release();
			}
			mCamera = null;
			if (mFrameChain != null) {
				mFrameChain[0].release();
				mFrameChain[1].release();

			}
			if (mCameraFrame != null) {
				mCameraFrame[0].release();
				mCameraFrame[1].release();
			}
		}
	}

	@Override
	protected boolean connectCamera(int width, int height) {

		/*
		 * 1. We need to instantiate camera 2. We need to start thread which
		 * will be getting frames
		 */
		/* First step - initialize camera connection */

		if (!initializeCamera(width, height))
			return false;

		/* now we can start update thread */

		mStopThread = false;
		mThread = new Thread(new CameraWorker());
		mThread.start();

		return true;
	}

	protected void disconnectCamera() {
		/*
		 * 1. We need to stop thread which updating the frames 2. Stop camera
		 * and release it
		 */

		try {
			mStopThread = true;

			synchronized (this) {
				this.notify();
			}

			if (mThread != null)
				mThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			mThread = null;
		}

		/* Now release camera */
		releaseCamera();
	}

	private class JavaCameraFrame implements CvCameraViewFrame {
		public Mat gray() {
			return mYuvFrameData.submat(0, mHeight, 0, mWidth);
		}

		public Mat rgba() {
			if (Setting.doPixelFormatOptimization) {

				Imgproc.cvtColor(mYuvFrameData, mRgba,
						Imgproc.COLOR_YUV2RGB_NV21);

			} else {
				Imgproc.cvtColor(mYuvFrameData, mRgba,
						Imgproc.COLOR_YUV2RGBA_NV21, 4);

			}
			return mRgba;

		}

		public JavaCameraFrame(Mat Yuv420sp, int width, int height) {
			super();
			mWidth = width;
			mHeight = height;
			mYuvFrameData = Yuv420sp;
			mRgba = new Mat();
		}

		public void release() {
			mRgba.release();
		}

		private Mat mYuvFrameData;
		private Mat mRgba;
		private int mWidth;
		private int mHeight;
	};

	/*
	 * public void setScaleByZoom(float scale){
	 * 
	 * float zoomRatio = mCamera.getParameters().getZoom()/10.0f+1;
	 * setScale(scale/zoomRatio); mZoomRatio = zoomRatio; }
	 */

	/**
	 * Set the scale of the CameraPreview.
	 * 
	 * @param scale
	 */
	public void setScale(float scale) {
		mScale = scale;
	}

	/**
	 * Set the zoom value of the camera.
	 * 
	 * @param zoomValue
	 */
	public void setZoom(int zoomValue) {

		Parameters param = mCamera.getParameters();
		if (param.isZoomSupported()) {
			Log.d("ZoomLevel", "setZoom = "+zoomValue);
			int max = param.getMaxZoom();
			zoomValue = Math.min(zoomValue, max);

			param.setZoom(zoomValue);
			mCamera.setParameters(param);

		} else {
			// TODO need to do something when setZoom is not supported.
		}

	}

	/**
	 * Given the progress of zoombar, set the best zoom and scale. <br>
	 * The strategy is first increase the zoom value to the max as the progress
	 * increases in the first half of zoombar. Then it increases the scale of
	 * camera view from Setting.BASIC_SCALE to
	 * Setting.BASIC_SCALE*Setting.MAX_SCALE_FACTOR.
	 * 
	 * @param progress
	 *            Percentage of the progress. Minimum progress is 0 and maximum
	 *            progress is 1;
	 */
	public void setBestZoomAndScale(float progress) {
		if (mCamera != null) {
			Parameters params = mCamera.getParameters();
			if (params != null) {
				if (progress <= 0.5) {

					int zoomValue = (int) (progress * 2 * params.getMaxZoom());
					float zoomRatio = zoomValue / 10.0f + 1;
					mZoomRatio = zoomRatio;
					setScale(Setting.BASIC_SCALE);
					if(Setting.smoothZooming){
						
						zoomValue = (int) Math.pow((double)params.getMaxZoom(), (double)(zoomValue)/(double)params.getMaxZoom());
					}
					setZoom(zoomValue);
					mMagnification = Setting.BASIC_SCALE * zoomRatio;
				} else {
					setZoom(params.getMaxZoom());
					mZoomRatio = mCamera.getParameters().getMaxZoom() / 10.0f + 1;
					float scale = Setting.BASIC_SCALE + 2 * Setting.BASIC_SCALE
							* (Setting.MAX_SCALE_FACTOR - 1)
							* (progress - 0.5f);
					setScale(scale);
					mMagnification = mZoomRatio * scale;
				}
			}

		}

	}

	/**
	 * @deprecated Because shifting the mat takes up too much time.
	 * @param addrGray
	 * @param addrRgba
	 * @param addrColorMat
	 * @param cvWindowSize
	 * @return
	 */
	public native float MotionEstimation(long addrGray, long addrRgba,
			long addrColorMat, int cvWindowSize);

	/**
	 * @deprecated resetMotion is done in java.
	 * @see {@link #reset()}
	 */
	public native void resetMotion();

	/**
	 * @deprecated
	 * @param addrRgba
	 * @param cvWindowSize
	 */
	public native void simpleShow(long addrRgba, int cvWindowSize);

	/**
	 * @deprecated FPS is calculated using FPSMeter of OpenCV.
	 * @param addrRgba
	 * @param fps
	 */
	public native void putFPS(long addrRgba, int fps);

	/**
	 * Hand written code to convert RGB to GREY. This is 20 times slower than
	 * using OpenCV method.
	 * 
	 * @param addrRgba
	 */
	public native void RGB2GREY(long addrRgba);

	/**
	 * Given the current frame and previous frame, calculates the motion between
	 * them.
	 * 
	 * @param lastMat
	 *            previsou frame
	 * @param currentMat
	 *            current frame
	 * @return float[] that contains the x and y motion.
	 */
	public native float[] MotionEstimation2(long lastMat, long currentMat);
	
	interface OnCameraViewStartedListener {
		public void onCameraViewStarted();
	}
	
	private OnCameraViewStartedListener mOnCameraViewStartedListener;
	
	public void setOnCameraViewStartedListener(OnCameraViewStartedListener mListener){
		mOnCameraViewStartedListener = mListener;
	}

	public CvCameraViewListener2 mCameraViewListener = new CvCameraViewListener2() {

		@Override
		public void onCameraViewStopped() {

		}

		@Override
		public void onCameraViewStarted(int width, int height) {
			// setFlashLight(isFlashOn);
			Log.d("RestoreState", "CameraViewStarted!");
			mOnCameraViewStartedListener.onCameraViewStarted();
			
		}

		@Override
		public Mat onCameraFrame(CvCameraViewFrame inputFrame) {

			/*
			 * float[] output = new float[3];
			 * mCamera.getParameters().getFocusDistances(output);
			 * 
			 * for(int i = 0 ; i < output.length; i++){ Log.d("Focus Distance",
			 * "Distance"+i+":"+ output[i]); }
			 */

			long startTime = System.currentTimeMillis();
			if (!isFrozen) {
				if (shouldEstablize) {
					getOpiticalFocusDistance(getCamera());
					Size detectWindowSize = new Size(128, 128);
					Rect detectRect = new Rect(
							(int) (mFrameWidth / 2 - detectWindowSize.width / 2),
							(int) (mFrameHeight / 2 - detectWindowSize.height / 2),
							(int) (detectWindowSize.width),
							(int) (detectWindowSize.height));

					Mat currGray = inputFrame.gray();
					Mat currRgba = null;
					if (Setting.filterMethod == Setting.FILTER_CVTCOLOR) {
						switch (Setting.enhancingMode) {
						case Setting.ENHANCING_NORMAL:
							currRgba = inputFrame.rgba();
							break;
						case Setting.ENHANCING_RGB2GRAY:
							currRgba = currGray;
							break;
						default:
							break;
						}
					} else {
						currRgba = inputFrame.rgba();
					}

					Mat subCurrGray = currGray.submat(detectRect);
					if (prevMat != null) {
						float[] result = MotionEstimation2(
								prevMat.getNativeObjAddr(),
								subCurrGray.getNativeObjAddr());
						if (isHorizontalLocked
								|| mOrientationMode == MainActivity.ORIENTATION_DOWN
								|| mOrientationMode == MainActivity.ORIENTATION_UP) {
							motionX += result[0];
							motionX = restrictInsideBoundary(motionX,
									mScreenWidth, mFrameWidth);

						}

						if (isHorizontalLocked
								|| mOrientationMode == MainActivity.ORIENTATION_LEFT
								|| mOrientationMode == MainActivity.ORIENTATION_RIGHT) {
							motionY += result[1];
							motionY = restrictInsideBoundary(motionY,
									mScreenHeight, mFrameHeight);

						}

					}

					prevMat = subCurrGray.clone();
					frozonMat = currRgba;

					if (Setting.filterMethod == Setting.FILTER_OPENCV
							&& Setting.enhancingMode == Setting.ENHANCING_RGB2GRAY) {
						startTime = System.currentTimeMillis();
						Imgproc.cvtColor(currRgba, currRgba,
								Imgproc.COLOR_BGR2GRAY);

					}
					return currRgba;
				} else {

					Mat currRgba = null;

					if (Setting.filterMethod != Setting.FILTER_CVTCOLOR) {
						currRgba = inputFrame.rgba();
					} else {
						switch (Setting.enhancingMode) {
						case Setting.ENHANCING_NORMAL:
							currRgba = inputFrame.rgba();
							break;
						case Setting.ENHANCING_RGB2GRAY:
							currRgba = inputFrame.gray();
							break;
						default:
							break;
						}
					}

					if (Setting.filterMethod == Setting.FILTER_OPENCV
							&& Setting.enhancingMode == Setting.ENHANCING_RGB2GRAY) {
						startTime = System.currentTimeMillis();
						Imgproc.cvtColor(currRgba, currRgba,
								Imgproc.COLOR_BGR2GRAY);
						// RGB2GREY(currRgba.nativeObj);
					}
					return currRgba;
				}
			} else {
				return (frozonMat == null) ? null : frozonMat;
			}
		}
	};

	/**
	 * Restricts the CameraPreview inside boundaries.
	 * 
	 * @param motion
	 *            motion of one direction.
	 * @param screenSize
	 *            screen size on that direction
	 * @param frameSize
	 *            the frame size on that direction
	 * @return new motion value that will not go out of boundary.
	 */
	private float restrictInsideBoundary(float motion, int screenSize,
			int frameSize) {
		if (Setting.ALLOW_OUT_OF_BOUNDARY) {
			return motion;
		} else {
			float boundary = ((mScale * frameSize - screenSize) / (2 * mScale));
			if (boundary > 0) {
				return Math.max(Math.min(motion, boundary), boundary * -1);
			} else {
				return Math.max(Math.min(motion, -boundary), boundary);
			}
		}
	}

	@Override
	public void scrollBy(int x, int y) {

		motionX += x;
		motionY += y;
	}

	public float getMotionX() {
		return motionX;
	}

	public float getMotionY() {
		return motionY;
	}

	private class CameraWorker implements Runnable {

		public void run() {
			do {
				synchronized (CameraPreview.this) {
					try {
						CameraPreview.this.wait();

					} catch (InterruptedException e) {

						e.printStackTrace();
					}
				}

				if (!mStopThread) {
					if (!mFrameChain[mChainIdx].empty())
						deliverAndDrawFrame(mCameraFrame[mChainIdx]);
					mChainIdx = 1 - mChainIdx;
				}
			} while (!mStopThread);

		}
	}

	public void onPreviewFrame(byte[] frame, Camera arg1) {

		// Log.d(TAG, " received. Frame size: " + frame.length);
		synchronized (this) {
			mFrameChain[1 - mChainIdx].put(0, 0, frame);

			this.notify();
		}
		if (mCamera != null)
			mCamera.addCallbackBuffer(mBuffer);
	}

	/**
	 * Reset the stabilization.
	 */
	public void reset() {
		motionX = 0;
		motionY = 0;

	}

	/**
	 * Start or stop stabilization based on the given boolean
	 * 
	 * @param shouldStart
	 *            true to enable the stabilization.
	 */
	public void setStabilization(boolean shouldStart) {
		shouldEstablize = shouldStart;
		prevMat = null;
	}

	/**
	 * Given the touch position, start auto focus at that position.
	 * 
	 * @param x
	 * @param y
	 */
	public void startAutoFocus(float x, float y) throws RuntimeException {
		if (supportFocus) {
			
			if (mCamera != null) {
				mCamera.cancelAutoFocus();
				android.graphics.Rect focusRect = calculateTapArea(x, y, 1f);
				android.graphics.Rect meteringRect = calculateTapArea(x, y,
						1.5f);
				Camera.Parameters params = mCamera.getParameters();
				params.setFocusMode(Parameters.FOCUS_MODE_AUTO);
				
				
				ArrayList<Camera.Area> focusAreas = new ArrayList<Camera.Area>();
				ArrayList<Camera.Area> meteringAreas = new ArrayList<Camera.Area>();
				focusAreas.add(new Area(focusRect, 1000));
				meteringAreas.add(new Area(meteringRect, 1000));
				params.setFocusAreas(focusAreas);
				params.setMeteringAreas(meteringAreas);
				mCamera.setParameters(params);
				mCamera.autoFocus(new AutoFocusCallback() {

					public void onAutoFocus(boolean success, Camera camera) {

						if (success) {
							UMENGhelper.onUserFocus(getContext(),
									getOpiticalFocusDistance(camera));
						} else {
							Log.d("AutoFocus", "Focus Fail");
						}

						Log.d("AutoFocus", "Autofocus finish!");

					}
				});

			}
		}

	}

	public float getOpiticalFocusDistance(Camera camera) {
		try{
			float[] focusDistance = new float[3];
			camera.getParameters().getFocusDistances(focusDistance);
			Log.d("AutoFocus", "getOpticalFocusDistance" + focusDistance[0] + " "
					+ focusDistance[1] + " " + focusDistance[2]);
			return focusDistance[1];
		} catch(Exception e){
			Log.d("NullPointerLog", Log.getStackTraceString(e));
		}
		return 0;
		
	}

	/**
	 * Calculates the focus and metering area based on the position and relative
	 * area size.
	 * 
	 * @param x
	 * @param y
	 * @param f
	 *            relative size of the area.
	 * @return Rect that is to be set as focus area or metering area.
	 */
	private android.graphics.Rect calculateTapArea(float x, float y, float f) {
		int width = getWidth();
		int height = getHeight();
		float scale = mScale;
		Camera.Size size = mCamera.getParameters().getPreviewSize();
		int previewHeight = size.height;
		int previewWidth = size.width;
		float focusX = (x - width / 2) / (scale * previewWidth) * 2000;
		float focusY = (y - height / 2) / (scale * previewHeight) * 2000;

		int focusWidth = (int) (Setting.FOCUS_WIDTH * f / scale);
		int focusHeight = focusWidth * previewWidth / previewHeight;

		focusX = Math.min(1000 - focusWidth / 2,
				Math.max(-1000 + focusWidth / 2, focusX));
		focusY = Math.min(1000 - focusHeight / 2,
				Math.max(-1000 + focusHeight / 2, focusY));

		return new android.graphics.Rect((int) (focusX - focusWidth / 2),
				(int) (focusY - focusHeight / 2),
				(int) (focusX + focusWidth / 2),
				(int) (focusY + focusHeight / 2));
	}

	/**
	 * Controls the flash light.
	 * 
	 * @param turnOnFlash
	 *            True then turn on the flashlight.
	 */
	public void setFlashLight(boolean turnOnFlash) {
		Log.d("FlashLight", turnOnFlash + "");
		try {

			Camera.Parameters params = mCamera.getParameters();
			if (turnOnFlash) {
				isFlashOn = true;
				params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
				mCamera.setParameters(params);
				Log.d("FlashLight", "FlashLight on!");

			} else {
				isFlashOn = false;
				params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
				mCamera.setParameters(params);

			}

		} catch (Exception e) {
			Log.d("NullPointerLog", Log.getStackTraceString(e));
			isFlashOn = false;
		}

	}

	public boolean isFlashLightOn() {
		return isFlashOn;
	}

	/**
	 * Controls the Horizontal lock.
	 * 
	 * @param lock
	 *            true then lock the horizontal motion.
	 */
	public void setHorizontalLock(boolean lock) {

		isHorizontalLocked = lock;

	}

	public boolean isHorizontalLocked() {
		return isHorizontalLocked;
	}

	public Camera getCamera() {
		return mCamera;
	}

	public float getScale() {
		return mScale;
	}

	/**
	 * If the phone is Samsung Galaxy SIII, then shows the frozen frame but
	 * keeps rendering the CameraPreview. Otherwise, shows the frozen frame and
	 * stop rendering CameraPreview. This means on scrolling or scaling can be
	 * done after the screen is frozen..
	 */
	public void setFrozen(boolean frozen) {
		if (!Compatibility.isGalaxyS3()) {
			super.setFrozen(frozen);
		}
		isFrozen = frozen;

	}

	/**
	 * Update the orientation mode of the phone. This should be called every
	 * time there is update of orientation to make sure the horizontal lock
	 * works properly
	 * 
	 * @param mode
	 */
	public void setOrientationMode(int mode) {
		mOrientationMode = mode;
	}

	public float getMagnification() {
		return mScale * mZoomRatio;
	}

	/**
	 * Given the progress and the maxprogress of the zoombar, set the
	 * corresponding scale.
	 * 
	 * @param progress
	 *            progress of zoombar.
	 * @param max
	 *            max progress of zoombar.
	 */
	public void setScaleForSIII(int progress, int max) {
		float progressScale = (((float) progress / (float) max)
				* (Setting.MAX_SCALE_FACTOR - 1) + 1)
				* Setting.BASIC_SCALE;
		setScale(progressScale);

	}

	/**
	 * Move the CameraPreview by x and y. This operation will not go out of
	 * boundary.
	 * 
	 * @param x
	 * @param y
	 */
	public void setMotionDiff(int x, int y) {
		motionX += x / mScale;
		motionY += y / mScale;
		motionX = restrictInsideBoundary(motionX, mScreenWidth, mFrameWidth);
		motionY = restrictInsideBoundary(motionY, mScreenHeight, mFrameHeight);

	}

	/**
	 * Move the CameraPreview to x and y.
	 * 
	 * @param x
	 * @param y
	 */
	public void setMotion(int x, int y) {
		motionX = x / mScale;
		motionY = y / mScale;
	}

	public void releaseFrozonMat() {
		// frozonMat = null;

	}

}
