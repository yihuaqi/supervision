package edu.harvard.schepens.supervisionv7;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

/**
 * This is the dialog fragment that asks user for description of snapshot to be
 * shared on facebook.
 * 
 * @author Huaqi Yi
 * 
 */
public class MyDialogFragment extends DialogFragment {
	Context mContext;
	EditText input;

	private Dialog mDialog = null;
	OnButtonClickedListener mListener;

	public MyDialogFragment() {

	}

	public interface OnButtonClickedListener {
		public void OnClicked(String description);
	}

	@Override
	public void onAttach(Activity activity) {

		super.onAttach(activity);
		try {
			mListener = (OnButtonClickedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnButtonClickedListener.");
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d("Facebook_sdk", "OnCreateView");

		return super.onCreateView(inflater, container, savedInstanceState);
	}

}
