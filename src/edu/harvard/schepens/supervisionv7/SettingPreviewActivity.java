package edu.harvard.schepens.supervisionv7;

import android.app.Activity;
import android.content.SharedPreferences;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

/**
 * Activity that allows user to select preview size.
 * 
 * @author Huaqi Yi
 * 
 */
public class SettingPreviewActivity extends Activity {
	LinearLayout previewSizeLinearLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.preview_size);
		previewSizeLinearLayout = (LinearLayout) findViewById(R.id.preview_size_layout);
		inflateRadioGroup();
	}

	/**
	 * Creates a radiogroup of supported preview sizes.
	 */
	private void inflateRadioGroup() {
		RadioGroup radioGroup = new RadioGroup(this);

		for (int i = 0; i < Setting.supportedPreviewSizes.size(); i++) {
			RadioButton button = new RadioButton(this);
			Size size = Setting.supportedPreviewSizes.get(i);
			button.setText(size.width + "X" + size.height);
			button.setId(i);
			if (!Setting.preview_auto) {
				if (size.width == Setting.preview_width
						&& size.height == Setting.preview_height) {
					button.setChecked(true);
				}
			}
			radioGroup.addView(button);

		}
		RadioButton autoButton = new RadioButton(this);
		if (Setting.preview_auto) {
			autoButton.setChecked(true);
		}
		autoButton.setId(Setting.supportedPreviewSizes.size());
		autoButton.setText("Auto");
		radioGroup.addView(autoButton);
		previewSizeLinearLayout.addView(radioGroup);
		radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				if (checkedId < Setting.supportedPreviewSizes.size()) {
					Size chosenSize = Setting.supportedPreviewSizes
							.get(checkedId);
					Log.d("Radio_Button", "Chose " + chosenSize.width + "X"
							+ chosenSize.height);
					Setting.preview_auto = false;
					Setting.preview_width = chosenSize.width;
					Setting.preview_height = chosenSize.height;
				} else if (checkedId == Setting.supportedPreviewSizes.size()) {
					Setting.preview_auto = true;
				}

			}
		});
	}

	@Override
	protected void onPause() {

		super.onPause();
		SharedPreferences.Editor editor = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext()).edit();
		editor.putBoolean(Setting.PREFERENCE_PREVIEW_IS_AUTO,
				Setting.preview_auto);
		editor.putInt(Setting.PREFERENCE_PREVIEW_HEIGHT, Setting.preview_height);
		editor.putInt(Setting.PREFERENCE_PREVIEW_WIDTH, Setting.preview_width);
		editor.putBoolean(Setting.PREFERENCE_SHOULD_SEND_FOCUS,
				Setting.shouldSendFocus);
		editor.commit();
	}

}
