	package edu.harvard.schepens.supervisionv7;



import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

/**
 * This activity is used for configuring user's subject id and group id.
 * 
 * @author Huaqi Yi
 *
 */
public class AuthorizationActivity extends Activity{
	Button startTestBtn;
	EditText authorizationEditText;
	TextView statusText;
	TextView groupStatusText;
	EditText groupEditText;
	Button groupButton;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.authorization);
		startTestBtn = (Button) findViewById(R.id.authorization_submit_btn);
		groupButton = (Button) findViewById(R.id.authorization_group_submit_btn);
		groupEditText = (EditText) findViewById(R.id.authorization_group_edittext);
		authorizationEditText = (EditText) findViewById(R.id.authorization_edittext);
		statusText = (TextView) findViewById(R.id.authorization_status);
		groupStatusText = (TextView) findViewById(R.id.authorization_group_status );
		if(!Setting.isSubject){
			statusText.setText("You're not a volunteer yet.");
			showGroup(false);
			
		} else {
			statusText.setText("Your volunteer id is "+Setting.subjectId);
			showGroup(true);
		}
		startTestBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if(v==startTestBtn){
					
					Setting.invalidateSubjectId(authorizationEditText.getText().toString());

						if(Setting.isSubject){
							
							Toast.makeText(AuthorizationActivity.this, "Subject Id:"+Setting.subjectId, Toast.LENGTH_LONG).show();
							showGroup(true);
							
						} else {
							
							Toast.makeText(AuthorizationActivity.this, "Invalid Subject Id.", Toast.LENGTH_LONG).show();
							showGroup(false); 	
						}
						showSubject();

					
				}
				
			}
		});
		
		groupButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(v==groupButton){
					
					Setting.validateGroup(groupEditText.getText().toString());
					if(Setting.isInGroup){
						
						Toast.makeText(AuthorizationActivity.this, "Group Id:"+Setting.groupId, Toast.LENGTH_LONG).show();
						
						
					} else {
						
						Toast.makeText(AuthorizationActivity.this, "Invalid Group Id.", Toast.LENGTH_LONG).show();
						 	
					}
					showGroup(true);
				}
				
			}
		});
	}

	/**
	 * Show the group information or hide based on the given boolean.
	 * @param shouldShow
	 */
	private void showGroup(boolean shouldShow) {
		if(shouldShow){
			groupButton.setVisibility(View.VISIBLE);
			groupEditText.setVisibility(View.VISIBLE);
			groupStatusText.setVisibility(View.VISIBLE);
			if(Setting.isInGroup){
				groupStatusText.setText("You're in group "+Setting.groupId);
			} else {
				groupStatusText.setText("You're not in any group.");
			}
		} else {
			groupButton.setVisibility(View.GONE);
			groupEditText.setVisibility(View.GONE);
			groupStatusText.setVisibility(View.GONE);
		}
		
	}
	
	/**
	 * Show the subject information.
	 */
	private void showSubject(){
		if(Setting.isSubject){
			statusText.setText("Your volunteer id is "+Setting.subjectId);
		} else {
			statusText.setText("You're not a volunteer yet.");
		}
	}

	public void savePreference() {
		SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
		editor.putBoolean(Setting.PREFERENCE_IS_SUBJECT, Setting.isSubject);
		editor.putString(Setting.PREFERENCE_SUBJECT_ID, Setting.subjectId);
		editor.putBoolean(Setting.PREFERENCE_IS_IN_GROUP, Setting.isInGroup);
		editor.putString(Setting.PREFERENCE_GROUP_ID, Setting.groupId);
		editor.commit();
	}

	@Override
	protected void onPause() {
		
		super.onPause();
		savePreference();
	}



}
