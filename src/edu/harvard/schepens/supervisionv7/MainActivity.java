package edu.harvard.schepens.supervisionv7;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Camera.Area;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.text.format.Time;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.testin.agent.TestinAgent;

import edu.harvard.schepens.supervisionv7.CameraPreview.OnCameraViewStartedListener;

//import com.sromku.simple.fb.Permission;
//import com.sromku.simple.fb.Permission.Type;
//import com.sromku.simple.fb.SimpleFacebook;
//import com.sromku.simple.fb.SimpleFacebookConfiguration;
//import com.sromku.simple.fb.entities.Photo;
//import com.sromku.simple.fb.entities.Privacy;
//import com.sromku.simple.fb.entities.Privacy.PrivacySettings;
//import com.sromku.simple.fb.listeners.OnLoginListener;
//import com.sromku.simple.fb.listeners.OnPublishListener;

/**
 * Main entrance of the app. This activity handles the buttons, gestures and
 * preferences.
 * 
 * @author Huaqi Yi
 * 
 */
public class MainActivity extends FragmentActivity {
	/**
	 * Customized CameraPreview to display camera preview.
	 */
	private CameraPreview mCameraPreview;

	/**
	 * Seekbar for adjust magnification.
	 */
	private SeekBar mZoombar;

	/**
	 * Button for Horizontal lock
	 */
	private Button mHorizontalLockBtn;

	/**
	 * Button for Flash light
	 */
	private Button mFlashBtn;

	/**
	 * Button for Snapshot.
	 */
	private Button mSnapshotBtn;

	/**
	 * Button for saving image
	 */
	private Button mSavePictureBtn;

	/**
	 * Button for sharing image
	 */
	private Button mSharePictureBtn;
	/**
	 * Button for open info page.
	 */
	private Button mInfoBtn;

	/**
	 * ImageView for displaying snapshot.
	 */
	private ImageView mSnapShotView;

	/**
	 * Flag that indicate if the image is frozen or not.
	 */
	private boolean isImageLocked = false;

	/**
	 * Current zoom level of camera. Used for restore state.
	 */
	private int mCameraZoom = -1;

	/**
	 * Flag that indicates if the flashlight is on or not.
	 */
	private boolean isFlashOn = false;

	/**
	 * Flag that indicates if the Horizontal lock is on or not.
	 */
	private boolean isHorizontalLocked = true;

	/**
	 * Current progress of {@link #mZoombar}.
	 */
	private int mProgress = 0;

	/**
	 * Supported preview size of camera.
	 */
	Camera.Size previewSize;

	/**
	 * Listener for checking the orientation change of device.
	 */
	private MyOrientationEventListener myOrientationEventListener;

	

	/**
	 * GalleryManager handles saving pictures, reading pictures, and rotating
	 * pictures.
	 */
	private GalleryManager mGalleryManager;

	/**
	 * Current orientation of the device. <br>
	 * For most of the phones, {@link #ORIENTATION_DOWN} is the orientation of
	 * portrait mode. But for some tablets {@link #ORIENTATION_DOWN} is the
	 * orientation of landscape mode. This results in a confusing layout on
	 * tablets.
	 * <p>
	 * Possible values: {@link #ORIENTATION_DOWN}, {@link #ORIENTATION_RIGHT},
	 * {@link #ORIENTATION_UP}, {@link #ORIENTATION_LEFT}.
	 */
	private int mOrientationMode = -1;

	/**
	 * Default orientation when phone is in portrait mode.
	 */
	public static final int ORIENTATION_DOWN = 0;
	/**
	 * 
	 * 180 degrees reverse of {@link #ORIENTATION_LEFT} ;
	 */
	public static final int ORIENTATION_RIGHT = 1;
	/**
	 * 180 degrees reverse of {@link #ORIENTATION_DOWN} ;
	 */
	public static final int ORIENTATION_UP = 2;
	/**
	 * Default orientation when phone is in landscape mode.
	 */
	public static final int ORIENTATION_LEFT = 3;

	// private SimpleFacebook mSimpleFacebook;

	/**
	 * Layout that contains: {@link #mHorizontalLockBtn}, {@link #mFlashBtn},
	 * {@link #mSnapshotBtn}, {@link #mInfoBtn}, {@link #mZoombar} .
	 */
	private RelativeLayout mWidgetsLayout;

	private Social mSocialController;

	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {

		@Override
		public void onManagerConnected(int status) {
			switch (status) {
			case LoaderCallbackInterface.SUCCESS: {

				// if (isImageLocked && Compatibility.isGalaxyS3()) {
				// mCameraPreview.setVisibility(View.VISIBLE);
				// // MobclickAgent.onEventEnd(MainActivity.this,
				// // Setting.EVENTID_LOCKSCREEN);
				// UMENGhelper.onLockscreenEnd(MainActivity.this);
				// isImageLocked = false;
				// mCameraPreview.reset();
				// mCameraPreview.setFrozen(false);
				// mSnapshotBtn.setBackground(getResources().getDrawable(
				// R.drawable.unlocked));
				// // Log.d("LockState",
				// // "Set to unlocked in onManagerConnected");
				// }
				mCameraPreview.enableView();
				//restoreState();
				if (Setting.enableFpsMeter) {
					mCameraPreview.enableFpsMeter();
				}
				// System.loadLibrary("opencv_java");
				System.loadLibrary("SuperVisionv6");
			}
				break;
			default: {

				super.onManagerConnected(status);
			}
				break;
			}
		}

	};
	
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		TestinAgent.init(this, Setting.APPKEY_360);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.activity_main);

		initWidgets();


		UMENGhelper.onCreate(this);
		mSocialController = new Social(MainActivity.this);
		mSocialController.onCreate(this, savedInstanceState);
		mSocialController.Init(mSharePictureBtn);
		// setupSocial(savedInstanceState);
		mGalleryManager = GalleryManager.getInstance();
		mGalleryManager.setActivity(MainActivity.this);

	}

	// private void setupSocial(Bundle savedInstanceState) {
	// Permission[] permissions = new Permission[] { Permission.PUBLISH_ACTION
	// };
	// SimpleFacebookConfiguration configuration = new
	// SimpleFacebookConfiguration.Builder()
	// // .setAppId(getResources().getString(R.string.facebook_app_id))
	// .setAppId("1483150318589301").setNamespace("supervisionplus")
	// .setPermissions(permissions).build();
	// SimpleFacebook.setConfiguration(configuration);
	//
	// }

	/**
	 * Initialize all the widgets used in this activity.
	 */
	private void initWidgets() {
		mZoombar = (SeekBar) findViewById(R.id.zoom_bar);
		mZoombar.setOnSeekBarChangeListener(mZoomListener);

		mFlashBtn = (Button) findViewById(R.id.flash_btn);
		mHorizontalLockBtn = (Button) findViewById(R.id.horizontal_lock_btn);
		mSnapshotBtn = (Button) findViewById(R.id.image_lock_btn);
		mInfoBtn = (Button) findViewById(R.id.info_btn);
		mInfoBtn.setOnClickListener(mOnClickListener);
		mFlashBtn.setOnClickListener(mOnClickListener);
		mHorizontalLockBtn.setOnClickListener(mOnClickListener);
		mSnapshotBtn.setOnClickListener(mOnClickListener);
		mSavePictureBtn = (Button) findViewById(R.id.save_image_btn);
		mSavePictureBtn.setOnClickListener(mOnClickListener);
		mSharePictureBtn = (Button) findViewById(R.id.share_image_btn);
		mSharePictureBtn.setOnClickListener(mOnClickListener);

		mSnapShotView = (ImageView) findViewById(R.id.snap_shot_imageview);
		mSnapShotView.setOnTouchListener(mSnapShotTouchListener);

		mCameraPreview = (CameraPreview) findViewById(R.id.camera_preview);
		mCameraPreview
				.setCvCameraViewListener(mCameraPreview.mCameraViewListener);
		mCameraPreview.setOnCameraViewStartedListener(new OnCameraViewStartedListener() {
			
			@Override
			public void onCameraViewStarted() {
				restoreState();
				
			}
		}); 
		mCameraPreview.setOnTouchListener(mCameraPreviewListener);
		/*
		 * This line is critical. Without this line the UI will not appear on
		 * rotation.
		 */
		mCameraPreview.setWillNotDraw(false);
		mCameraPreview.setMaxFrameSize(Setting.MAX_FRAME_WIDTH,
				Setting.MAX_FRAME_HEIGHT);

		mWidgetsLayout = (RelativeLayout) findViewById(R.id.layout_widgets);

		myOrientationEventListener = new MyOrientationEventListener(this);
		/* Initialize the layout. */
		myOrientationEventListener.onOrientationChanged(ORIENTATION_DOWN);

	}

	/**
	 * Load the preference of this app, including subjectId, and preview size
	 * setting.
	 */
	private void loadPreference() {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		Setting.isSubject = prefs.getBoolean(Setting.PREFERENCE_IS_SUBJECT,
				false);
		Setting.subjectId = prefs.getString(Setting.PREFERENCE_SUBJECT_ID,
				"9999");
		Setting.isInGroup = prefs.getBoolean(Setting.PREFERENCE_IS_IN_GROUP,
				false);
		Setting.groupId = prefs.getString(Setting.PREFERENCE_GROUP_ID, "9999");
		Setting.preview_auto = prefs.getBoolean(
				Setting.PREFERENCE_PREVIEW_IS_AUTO, true);
		Setting.preview_height = prefs.getInt(
				Setting.PREFERENCE_PREVIEW_HEIGHT, 0);
		Setting.preview_width = prefs.getInt(Setting.PREFERENCE_PREVIEW_WIDTH,
				0);
		Setting.shouldSendFocus = prefs.getBoolean(
				Setting.PREFERENCE_SHOULD_SEND_FOCUS, false);
		ActiveDaysCheck.checkActiveValue(prefs,this);
		//AppRater.resetAllValues(prefs.edit());
		//AppRater.app_launched(this, prefs);

	}
	

	
	

	/**
	 * This function is used to get the device information which is used for
	 * setting itself as a test device in Umeng.
	 * 
	 * @param context
	 * @return The device information which can be used in Umeng.
	 */
	public static String getDeviceInfo(Context context) {
		try {
			org.json.JSONObject json = new org.json.JSONObject();
			android.telephony.TelephonyManager tm = (android.telephony.TelephonyManager) context
					.getSystemService(Context.TELEPHONY_SERVICE);

			String device_id = tm.getDeviceId();

			android.net.wifi.WifiManager wifi = (android.net.wifi.WifiManager) context
					.getSystemService(Context.WIFI_SERVICE);

			String mac = wifi.getConnectionInfo().getMacAddress();
			json.put("mac", mac);

			if (TextUtils.isEmpty(device_id)) {
				device_id = mac;
			}

			if (TextUtils.isEmpty(device_id)) {
				device_id = android.provider.Settings.Secure.getString(
						context.getContentResolver(),
						android.provider.Settings.Secure.ANDROID_ID);
			}

			json.put("device_id", device_id);

			return json.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		mSocialController.onStart(this);
	}


	
	/**
	 * This OnTouchListener handles the pinch-to-zoom, focusing and stabilizing
	 * gesture when the camera preview is not frozen.
	 */
	private OnTouchListener mCameraPreviewListener = new OnTouchListener() {
		/**
		 * Mode of current operation.
		 * <p>
		 * Possible values: {@link #MODE_DRAG}, {@link #MODE_STABLIZE},
		 * {@link #MODE_ZOOM};
		 */
		private int mode = 0;
		/**
		 * Mode that should do stabilization.
		 */
		private final int MODE_STABLIZE = 0;

		/**
		 * Mode that should do pinch-to-zoom.
		 */
		private final int MODE_ZOOM = 1;

		/**
		 * Mode that should do dragging for Samsung Galaxy SIII after lock
		 * screen.
		 */
		private final int MODE_DRAG = 2;

		/**
		 * Time stamp of touch start. This is used for determining whether
		 * focusing will be done or not.
		 */
		long touchStartTimeStamp = 0;

		/**
		 * Previous distance of two finger. This is used for determining the
		 * zooming.
		 */
		private float prevDistance = 0;

		/**
		 * Used for Galaxy SIII to do the scrolling in locked screen.
		 */
		private float prevSIIIX = 0;
		/**
		 * Used for Galaxy SIII to do the scrolling in locked screen.
		 */
		private float prevSIIIY = 0;

		private boolean stablizedLongEnough = false;

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			// boolean isGalaxyS3 = Compatibility.isGalaxyS3();
			// if (isGalaxyS3 && isImageLocked) {
			// HandleGalaxyS3ImageLocked(event);
			//
			// return true;
			// }

			if (isImageLocked) {
				return false;
			}

			HandleCameraPreviewGesture(event);

			return true;
		}

		MyTimerAsyncTask mTimerAsyncTask;

		/**
		 * Handle the CameraPreview Gesture.
		 * 
		 * @param event
		 */
		private void HandleCameraPreviewGesture(MotionEvent event) {

			switch (event.getAction() & MotionEvent.ACTION_MASK) {
			/* Do stabilization if one finger is on the screen. */
			case MotionEvent.ACTION_DOWN:
				mTimerAsyncTask = new MyTimerAsyncTask();
				stablizedLongEnough = false;
				touchStartTimeStamp = System.currentTimeMillis();
				mCameraPreview.reset();
				mCameraPreview.setStabilization(true);

				String lockcondition = null;
				if (Compatibility.isGalaxyS3()) {
					prevSIIIX = event.getX();
					prevSIIIY = event.getY();
				}
				/* Horizontal lock is different for different orientation. */
				if (!mCameraPreview.isHorizontalLocked) {
					if (mOrientationMode == ORIENTATION_DOWN
							|| mOrientationMode == ORIENTATION_UP) {
						lockcondition = UMENGhelper.EVENTVALUE_HORIZONTALLY_UNLOCKED;
					} else if (mOrientationMode == ORIENTATION_LEFT
							|| mOrientationMode == ORIENTATION_RIGHT) {
						lockcondition = UMENGhelper.EVENTVALUE_VERTICALLY_UNLOCKED;
					}
				} else {
					lockcondition = UMENGhelper.EVENTVALUE_ALL_LOCKED;
				}
				mTimerAsyncTask.execute(lockcondition);
				break;

			case MotionEvent.ACTION_UP:
				/* Short touch will trigger focusing. */
				long touchEndTimeStamp = System.currentTimeMillis();
				if ((touchEndTimeStamp - touchStartTimeStamp) <= Setting.AUTO_FOCUS_INTERVAL) {
					try{
						mCameraPreview.startAutoFocus(event.getX(), event.getY());
					} catch(RuntimeException e){
						Log.d("AutoFocusLog",Log.getStackTraceString(e));
					}
				}
				Boolean iscancled = mTimerAsyncTask.cancel(true);
				Log.d("UM Stabilization", "isCanceled:" + iscancled);
				mCameraPreview.setStabilization(false);
				if (stablizedLongEnough) {
					Log.d("UM Stabilization", "Long enough:"
							+ stablizedLongEnough);
					UMENGhelper.onStabilizationEnd(MainActivity.this);
					UMENGhelper.onUserFocus(MainActivity.this, mCameraPreview
							.getOpiticalFocusDistance(mCameraPreview
									.getCamera()));
				}

				mCameraPreview.reset();
				break;
			case MotionEvent.ACTION_POINTER_DOWN:
				/* Enter pinch-to-zoom mode. */
				mCameraPreview.setStabilization(false);
				mode = MODE_ZOOM;
				float prevX = event.getX(0);
				float prevY = event.getY(0);
				float prevPointerX = event.getX(1);
				float prevPointerY = event.getY(1);
				prevDistance = (float) Math.sqrt((prevX - prevPointerX)
						* (prevX - prevPointerX) + (prevY - prevPointerY)
						* (prevY - prevPointerY));
				break;
			case MotionEvent.ACTION_POINTER_UP:
				mode = MODE_STABLIZE;
				mCameraPreview.setStabilization(true);
				break;
			case MotionEvent.ACTION_MOVE:
				if (mode == MODE_ZOOM) {
					float currX = event.getX(0);
					float currY = event.getY(0);
					float currPointerX = event.getX(1);
					float currPointerY = event.getY(1);
					float currDistance = (float) Math
							.sqrt((currX - currPointerX)
									* (currX - currPointerX)
									+ (currY - currPointerY)
									* (currY - currPointerY));
					float ratio = currDistance / prevDistance;
					int addProgress = (int) ((Math.log(ratio) / Math.log(2)) / 4 * mZoombar
							.getMax());
					int progress = Math.min(
							Math.max(addProgress + mZoombar.getProgress(), 0),
							mZoombar.getMax());
					mZoombar.setProgress(progress);
					if (Math.abs(addProgress) >= 1) {
						prevDistance = currDistance;
					}

				}

				break;

			default:
				break;
			}

		}

		class MyTimerAsyncTask extends AsyncTask<String, Void, Void> {
			String lockcondition = "null";

			@Override
			protected Void doInBackground(String... params) {
				lockcondition = params[0];
				try {
					Thread.sleep(Setting.STABILIZATION_THRESHOLD);
				} catch (Exception e) {
					// TODO: handle exception
				}
				return null;

			}

			@Override
			protected void onPostExecute(Void result) {
				Log.d("UM Stabilization", "onPostExecute");
				stablizedLongEnough = true;
				UMENGhelper.onStabilizationBegin(MainActivity.this,
						lockcondition);
			}

		}

		/**
		 * Handles the frozen camera preview as the imageview for Galaxy S3.
		 * 
		 * @param event
		 */
		private void HandleGalaxyS3ImageLocked(MotionEvent event) {
			switch (event.getAction() & MotionEvent.ACTION_MASK) {
			/* Do stabilization if one finger is on the screen. */
			case MotionEvent.ACTION_DOWN:
				mode = MODE_DRAG;
				if (Compatibility.isGalaxyS3()) {
					prevSIIIX = event.getX();
					prevSIIIY = event.getY();
				}
				break;
			case MotionEvent.ACTION_UP:
				break;
			case MotionEvent.ACTION_POINTER_DOWN:
				/* Enter pinch-to-zoom mode. */
				mode = MODE_ZOOM;
				float prevX = event.getX(0);
				float prevY = event.getY(0);
				float prevPointerX = event.getX(1);
				float prevPointerY = event.getY(1);
				prevDistance = (float) Math.sqrt((prevX - prevPointerX)
						* (prevX - prevPointerX) + (prevY - prevPointerY)
						* (prevY - prevPointerY));
				break;
			case MotionEvent.ACTION_MOVE:
				/*
				 * Drag the image or zoom in/out, depending on the current
				 * state.
				 */
				if (mode == MODE_ZOOM) {
					float currX = event.getX(0);
					float currY = event.getY(0);
					float currPointerX = event.getX(1);
					float currPointerY = event.getY(1);
					float currDistance = (float) Math
							.sqrt((currX - currPointerX)
									* (currX - currPointerX)
									+ (currY - currPointerY)
									* (currY - currPointerY));
					float ratio = currDistance / prevDistance;
					int addProgress = (int) ((Math.log(ratio) / Math.log(2)) / 4 * mZoombar
							.getMax());
					int progress = Math.min(
							Math.max(addProgress + mZoombar.getProgress(), 0),
							mZoombar.getMax());
					mZoombar.setProgress(progress);
					if (Math.abs(addProgress) >= 1) {
						prevDistance = currDistance;
					}

				}
				if (mode == MODE_DRAG) {
					int diffX = (int) (prevSIIIX - event.getX());
					int diffY = (int) (prevSIIIY - event.getY());

					mCameraPreview.setMotionDiff(diffX, diffY);
					// restrictInsideBoundary(mCameraPreview);

					if (Math.abs(diffX) >= 1) {
						prevSIIIX = event.getX();
					}
					if (Math.abs(diffY) >= 1) {
						prevSIIIY = event.getY();
					}
				}

				break;
			default:
				break;
			}

		}
	};

	/**
	 * TouchListener that handles the drag and pinch-to-zoom gesture of the
	 * SnapShotView.
	 */
	private OnTouchListener mSnapShotTouchListener = new OnTouchListener() {
		float prevX = 0;
		float prevY = 0;
		float prevRawX = 0;
		float prevRawY = 0;
		float prevPointerX = 0;
		float prevPointerY = 0;
		int firstPointerId = 0;
		int secondPointerId = 0;
		double prevDistance = 0;
		float prevScale = 0;
		int mode = 0;
		final int MODE_NONE = 0;
		final int MODE_DRAG = 1;
		final int MODE_ZOOM = 2;
		final int MODE_PREV = 3;
		final int MODE_NEXT = 4;
		float screenWidth = -1;
		float screenHeight = -1;

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			if (v == mSnapShotView) {

				switch (event.getAction() & MotionEvent.ACTION_MASK) {
				case MotionEvent.ACTION_DOWN:
					/* One finger: dragging. */
					prevX = event.getX();
					prevY = event.getY();
					prevRawX = event.getRawX();
					prevRawY = event.getRawY();
					if (screenHeight <= 0 || screenWidth <= 0) {
						Display mDisplay = getWindowManager()
								.getDefaultDisplay();
						screenWidth = mDisplay.getWidth();
						screenHeight = mDisplay.getHeight();
					}
					if (isPrevStart(mOrientationMode, prevRawX, prevRawY)) {
						mode = MODE_PREV;

					} else if (isNextStart(mOrientationMode, prevRawX, prevRawY)) {
						mode = MODE_NEXT;

					} else {
						mode = MODE_DRAG;
					}
					break;
				case MotionEvent.ACTION_MOVE:

					if (mode == MODE_DRAG) {
						int diffX = (int) (prevX - event.getX());
						int diffY = (int) (prevY - event.getY());

						mSnapShotView.scrollBy(diffX, diffY);
						restrictInsideBoundary(mSnapShotView);
						if (Math.abs(diffX) >= 1) {
							prevX = event.getX();
						}
						if (Math.abs(diffY) >= 1) {
							prevY = event.getY();
						}

					} else if (mode == MODE_ZOOM) {
						int firstPointerIndex = event
								.findPointerIndex(firstPointerId);
						int secondPointerIndex = event
								.findPointerIndex(secondPointerId);
						float currX = event.getX(firstPointerIndex);
						float currY = event.getY(firstPointerIndex);
						float currPointerX = event.getX(secondPointerIndex);
						float currPointerY = event.getY(secondPointerIndex);
						float currDistance = (float) Math
								.sqrt((currX - currPointerX)
										* (currX - currPointerX)
										+ (currY - currPointerY)
										* (currY - currPointerY))
								* mSnapShotView.getScaleX();
						float scale = (float) (prevScale * currDistance / prevDistance);

						int progress = mZoomListener.calculateProgress(scale);
						progress = Math.max(
								Math.min(progress, mZoombar.getMax()), 0);

						mZoombar.setProgress(progress);
						// mZoomListener.onProgressChanged(mZoombar, progress,
						// false);

					}
					break;
				case MotionEvent.ACTION_POINTER_DOWN:
					firstPointerId = event.getPointerId(0);
					secondPointerId = event.getPointerId(1);
					mode = MODE_ZOOM;
					prevX = event.getX(0);
					prevY = event.getY(0);

					prevPointerX = event.getX(1);
					prevPointerY = event.getY(1);
					prevScale = mSnapShotView.getScaleX();

					prevDistance = Math.sqrt((prevX - prevPointerX)
							* (prevX - prevPointerX) + (prevY - prevPointerY)
							* (prevY - prevPointerY))
							* prevScale;
					break;
				case MotionEvent.ACTION_POINTER_UP:
					// Avoid image jumping after zooming.
					mode = MODE_NONE;
					break;
				case MotionEvent.ACTION_UP:

					float currentRawY = event.getRawY();
					float currentRawX = event.getRawX();
					if (mode == MODE_NEXT) {
						if (isNextComplete(mOrientationMode, currentRawX,
								currentRawY, prevRawX, prevRawY)) {

							showNextBitmap();
						}
					} else if (mode == MODE_PREV) {
						if (isPrevComplete(mOrientationMode, currentRawX,
								currentRawY, prevRawX, prevRawY)) {

							showPrevBitmap();
						}
					}
					mode = MODE_NONE;
					break;
				default:
					break;
				}
				return true;
			} else {
				return false;
			}
		}

		/**
		 * Check whether a browsing previous snapshot gesture is completed.
		 * 
		 * @param orientationMode
		 *            current orientation mode of the phone
		 *            {@link MainActivity#mOrientationMode}
		 * @param endX
		 *            End raw X position
		 * @param endY
		 *            End raw Y position
		 * @param startX
		 *            Start raw X position
		 * @param startY
		 *            Start raw Y position
		 * @return true if the snapshot gesture is completed.
		 */
		private boolean isPrevComplete(int orientationMode, float endX,
				float endY, float startX, float startY) {

			return isNextComplete((orientationMode + 2) % 4, endX, endY,
					startX, startY);
		}

		/**
		 * Check whether a browsing next snapshot gesture is completed.
		 * 
		 * @param orientationMode
		 *            current orientation mode of the phone
		 *            {@link MainActivity#mOrientationMode}
		 * @param endX
		 *            End raw X position
		 * @param endY
		 *            End raw Y position
		 * @param startX
		 *            Start raw X position
		 * @param startY
		 *            Start raw Y position
		 * @return true if the snapshot gesture is completed.
		 */
		private boolean isNextComplete(int orientationMode, float endX,
				float endY, float startX, float startY) {

			switch (orientationMode) {
			case ORIENTATION_DOWN:
				return endY - startY >= screenHeight
						* Setting.BROWSING_GESTURE_END_THRESHOLD
						|| (endX - startX) <= -screenWidth
								* Setting.BROWSING_GESTURE_END_THRESHOLD;

			case ORIENTATION_LEFT:
				return endX - startX <= -screenWidth
						* Setting.BROWSING_GESTURE_END_THRESHOLD
						|| (endY - startY) <= -screenHeight
								* Setting.BROWSING_GESTURE_END_THRESHOLD;

			case ORIENTATION_RIGHT:
				return endX - startX >= screenWidth
						* Setting.BROWSING_GESTURE_END_THRESHOLD
						|| (endY - startY) >= screenHeight
								* Setting.BROWSING_GESTURE_END_THRESHOLD;
			case ORIENTATION_UP:
				return endY - startY <= -screenHeight
						* Setting.BROWSING_GESTURE_END_THRESHOLD
						|| (endX - startX) >= screenWidth
								* Setting.BROWSING_GESTURE_END_THRESHOLD;

			default:
				break;
			}
			return false;
		}

		/**
		 * Check whether this is a start of browsing next snapshot gesture or
		 * not
		 * 
		 * @param orientationMode
		 *            current orientation mode of the phone
		 *            {@link MainActivity#mOrientationMode}
		 * @param x
		 *            Raw X position
		 * @param y
		 *            Raw Y position
		 * @return true if the touch event happens in the start area.
		 */
		private boolean isNextStart(int orientationMode, float x, float y) {

			switch (orientationMode) {
			case ORIENTATION_DOWN:
				return y <= screenHeight
						* Setting.BROWSING_GESTURE_START_THRESHOLD
						|| x >= (1 - Setting.BROWSING_GESTURE_START_THRESHOLD)
								* screenWidth;

			case ORIENTATION_LEFT:
				return x >= screenWidth
						* (1 - Setting.BROWSING_GESTURE_START_THRESHOLD)
						|| y >= (1 - Setting.BROWSING_GESTURE_START_THRESHOLD)
								* screenHeight;
			case ORIENTATION_RIGHT:
				return x <= screenWidth
						* Setting.BROWSING_GESTURE_START_THRESHOLD
						|| y <= (Setting.BROWSING_GESTURE_START_THRESHOLD)
								* screenHeight;
			case ORIENTATION_UP:
				return y >= 9 * screenHeight
						* Setting.BROWSING_GESTURE_START_THRESHOLD
						|| x <= Setting.BROWSING_GESTURE_START_THRESHOLD
								* screenWidth;
			}
			return false;
		}

		/**
		 * Check whether this is a start of browsing previous snapshot gesture.
		 * 
		 * @param orientationMode
		 *            current orientation mode of the phone
		 *            {@link MainActivity#mOrientationMode}
		 * @param x
		 *            Raw X position
		 * @param y
		 *            Raw Y position
		 * @return true if the touch event is in the start area.
		 */
		private boolean isPrevStart(int orientationMode, float x, float y) {
			return isNextStart((orientationMode + 2) % 4, x, y);
		}

	};

	/**
	 * Listener that handles horizontal lock button, flashlight button, snapshot
	 * button, and info button.
	 */
	public OnClickListener mOnClickListener = new OnClickListener() {
		Camera mCamera;

		@Override
		public void onClick(View v) {
			int id = v.getId();
			if (id == R.id.flash_btn) {
				/* Turn on and off the flashlight */
				mCamera = mCameraPreview.getCamera();
				if (mCamera != null) {
					if (isFlashOn) {

						UMENGhelper.onFlashLightEnd(MainActivity.this);
						mCameraPreview.setFlashLight(false);
						mFlashBtn.setBackground(getResources().getDrawable(
								R.drawable.flashoff));
						isFlashOn = false;
						AppRater.app_launched(MainActivity.this);

					} else {

						UMENGhelper.onFlashLightBegin(MainActivity.this);
						mCameraPreview.setFlashLight(true);
						mFlashBtn.setBackground(getResources().getDrawable(
								R.drawable.flashon));
						isFlashOn = true;
					}
				}
			} else if (id == R.id.image_lock_btn) {
				handleImageLock();
			} else if (id == R.id.horizontal_lock_btn) {
				if (isHorizontalLocked) {
					mCameraPreview.setHorizontalLock(false);
					isHorizontalLocked = false;
					mHorizontalLockBtn.setBackground(getResources()
							.getDrawable(R.drawable.onestable));

				} else {

					mCameraPreview.setHorizontalLock(true);
					mHorizontalLockBtn.setBackground(getResources()
							.getDrawable(R.drawable.twostable));
					isHorizontalLocked = true;
				}
			} else if (id == R.id.info_btn) {
				UMENGhelper.onInfopage(MainActivity.this);
				Intent infoIntent = new Intent(MainActivity.this,
						InfoActivity.class);
				startActivity(infoIntent);
			} else if (id == R.id.save_image_btn) {
				savePicture();
				/* One snapshot only need to be saved for once. */
				mSavePictureBtn.setVisibility(View.INVISIBLE);

			} else if (id == R.id.share_image_btn) {

				mSocialController.onClickShare(MainActivity.this,
						mGalleryManager.getCurrentBitmap());

			}

		}

	};

	private boolean isHandlingImageLock = false;

	public void handleImageLock() {
		if (isHandlingImageLock) {
			return;
		}
		if (!isImageLocked) {
			isHandlingImageLock = true;
		}
		/* Lock and unlock the screen. */
		Camera mCamera = mCameraPreview.getCamera();
		mFlashBtn
				.setBackground(getResources().getDrawable(R.drawable.flashoff));
		if (isImageLocked) {
			/* Bring up the CameraPreview */
			mCameraPreview.setVisibility(View.VISIBLE);
			mSavePictureBtn.setVisibility(View.GONE);
			mSharePictureBtn.setVisibility(View.GONE);
			mFlashBtn.setVisibility(View.VISIBLE);
			mHorizontalLockBtn.setVisibility(View.VISIBLE);
			UMENGhelper.onLockscreenEnd(MainActivity.this);
			isImageLocked = false;
			mCameraPreview.reset();
			mCameraPreview.setFrozen(false);
			mCameraPreview.enableView();

			//restoreState();
			mSnapShotView.setVisibility(View.GONE);
			mSnapshotBtn.setBackground(getResources().getDrawable(
					R.drawable.unlocked));
			Log.d("LockState", "Set to unlocked in handleImageLock");
			AppRater.app_launched(this);

		} else {
			// To prevent user from clicking this button several times.
			mSnapshotBtn.setVisibility(View.INVISIBLE);
			mSnapshotBtn.setClickable(false);
			isImageLocked = true;

			mFlashBtn.setVisibility(View.GONE);
			mHorizontalLockBtn.setVisibility(View.GONE);
			saveState();
			UMENGhelper.onLockscreenBegin(MainActivity.this);
			UMENGhelper.onFlashLightEnd(MainActivity.this);
			if (mCamera != null) {
				if (!Compatibility.isGalaxyS3()) {
					isImageLocked = true;
					/*
					 * Lock the screen by taking a picture. The picture size
					 * should be the one that just above 1080p.
					 */
					Camera.Parameters params = mCamera.getParameters();
					List<Camera.Size> sizes = params.getSupportedPictureSizes();
					Runtime rt = Runtime.getRuntime();
					long maxMemory = rt.maxMemory();
					Camera.Size pictureSize = sizes.get(sizes.size() - 1);
					Log.d("OutOfMemoryLog", "Max Memory:" + maxMemory);
					ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
					int memoryClass = am.getMemoryClass();
					Log.v("OutOfMemoryLog",
							"memoryClass:" + Integer.toString(memoryClass));
					Log.v("OutOfMemoryLog",
							"LargerMemoryClass:" + Integer.toString(((ActivityManager)getSystemService(ACTIVITY_SERVICE)).getLargeMemoryClass()));
					
					
					
					int widthThreshold = 1080;
					int heightThreshold = 1920;
					int resultIndex = 0;
					
					int targetThreshold = widthThreshold * heightThreshold;
					int maxDiff = targetThreshold;
					
					
					boolean lowerResolution = memoryClass < 60 ;
					if(lowerResolution){
						targetThreshold = 1280 * 960;
					}
					
					for (int i = sizes.size() - 1; i >= 0; i--) {

						int diff =Math.abs(sizes.get(i).height*sizes.get(i).width - targetThreshold); 
						if(diff <= maxDiff){
							resultIndex = i;
							maxDiff = diff; 
						}
						
					}
					pictureSize = sizes.get(resultIndex);
					params.setPictureSize(pictureSize.width, pictureSize.height);
					Log.d("Picture Size", "Final:"+params.getPictureSize().height + ":"
							+ params.getPictureSize().width);
					Log.d("OutOfMemoryLog", "Picture Size:" + pictureSize.height + ":"
							+ pictureSize.width);
					mCamera.setParameters(params);
					mCameraPreview.setFrozen(true);

					executeTakePictureTask();

				} else {

					isImageLocked = true;
					Runtime rt = Runtime.getRuntime();
					long maxMemory = rt.maxMemory();

					Log.d("Max Memory", "Max Memory:" + maxMemory);
					ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
					int memoryClass = am.getMemoryClass();
					Log.v("Max Memory",
							"memoryClass:" + Integer.toString(memoryClass));

					/*
					 * The Galaxy SIII model has a bug in taking picture if the
					 * picture size is set after the preview starts. However if
					 * the picture size is set before the preview starts,
					 * setZoom() doesn't work properly. So here a workaround,
					 * which is only showing the frozen CameraPreview frame, is
					 * used.
					 */
					/*
					 * mCameraPreview.setFrozen(true);
					 * 
					 * UpdateZoombarOnLocked(); isImageLocked = true;
					 */
					Camera.Parameters params = mCamera.getParameters();
					List<Camera.Size> sizes = params.getSupportedPictureSizes();
					Camera.Size pictureSize = sizes.get(sizes.size() - 1);
					for (int i = sizes.size() - 1; i >= 0; i--) {
						pictureSize = sizes.get(i);
						if (sizes.get(i).height >= 1080) {
							break;
						}
					}
					params.setPictureSize(pictureSize.width, pictureSize.height);
					mCamera.setParameters(params);
					mCameraPreview.setFrozen(true);

					executeTakePictureTask();

				}
			}
			mSnapshotBtn.setBackground(getResources().getDrawable(
					R.drawable.locked));

			Log.d("LockState", "Set to locked in handleImageLock");

		}
	}

	/**
	 * Let the SnapshotView display the next bitmap that is saved as 
	 * saved in local disk. It will do nothing if it is called when SnapshoView
	 * is displaying the most recent snapshot.
	 */
	private void showNextBitmap() {
		Bitmap nextBitmap = mGalleryManager.getNextBitmap(mOrientationMode);
		if (nextBitmap != null) {

			mSnapShotView.setImageBitmap(nextBitmap);

		}
	}

	/**
	 * Let the SnapshotView display the previous bitmap
	 * saved in local disk. It will do nothing if it is called when SnapshoView
	 * is displaying the first snapshot that is ever taken.
	 */
	private void showPrevBitmap() {

		Bitmap prevBitmap = mGalleryManager.getPrevBitmap(mOrientationMode);
		if (prevBitmap != null) {

			mSnapShotView.setImageBitmap(prevBitmap);
		}
	}

	/**
	 * Calculate and update the progress of zoombar when the picture is locked.
	 */
	public void UpdateZoombarOnLocked() {
		int progress = (int) (((mCameraPreview.getScale() / Setting.BASIC_SCALE) - 1)
				* mZoombar.getMax() / ((Setting.MAX_ZOOMVALUE / 10.0f + 1)
				/ (mCameraZoom / 10.0f + 1) * (Setting.MAX_SCALE_FACTOR - 1)));

		mZoombar.setProgress(progress);
	}

	/**
	 * Handles the zoombar.
	 */
	private MyOnSeekBarChangeLisener mZoomListener = new MyOnSeekBarChangeLisener();

	class MyOnSeekBarChangeLisener implements OnSeekBarChangeListener {
		/*
		 * Scale the imageview by this ratio will scale the imageview to the
		 * size of picture size.
		 */
		private float pixelToPixelRatio = 1;
		/*
		 * Scale the imageview to the same scale of the preview if the imageview
		 * size is the same as the picture size.
		 */
		private float sameScaleRatio = 1;

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {

		}

		// float scale = (minScale +
		// (float)(progress)/seekBar.getMax()*(maxScale-minScale)*zoomScale)*pixelToPixelRatio*sameScaleRatio;

		/**
		 * Calculate the corresponding progress related to the given scale.
		 * 
		 * @param scale
		 *            The scale that is corresponding to the result progress.
		 * @return The corresponding progress value.
		 */
		public int calculateProgress(float scale) {
			float minScale = 1;
			float maxScale = Setting.MAX_SCALE_FACTOR;
			float zoomScale = 1;
			int progress = (int) ((scale / pixelToPixelRatio / sameScaleRatio - minScale)
					/ zoomScale / (maxScale - minScale) * 100);
			return progress;
		}

		/**
		 * This method will scale the imageview to the same size as picture
		 * 
		 * @param params
		 *            parameters of the camera
		 */
		public void setPixelToPixelRatio(Parameters params) {

			params.getPictureSize();
			Display display = getWindowManager().getDefaultDisplay();
			float screenHeight = display.getHeight();
			float screenWidth = display.getWidth();
			float pictureHeight = 0;
			float pictureWidth = 0;
			pictureHeight = mSnapShotView.getDrawable().getIntrinsicHeight();
			pictureWidth = mSnapShotView.getDrawable().getIntrinsicWidth();
			/*
			 * if (mOrientationMode == ORIENTATION_DOWN || mOrientationMode ==
			 * ORIENTATION_UP) { pictureHeight = mSnapShotView.getDrawable()
			 * .getIntrinsicHeight(); pictureWidth =
			 * mSnapShotView.getDrawable().getIntrinsicWidth(); } else {
			 * pictureHeight = mSnapShotView.getDrawable().getIntrinsicWidth();
			 * pictureWidth = mSnapShotView.getDrawable().getIntrinsicHeight();
			 * }
			 */

			if (pictureHeight <= screenHeight && pictureWidth <= screenWidth) {
				pixelToPixelRatio = 1;
				return;
			}

			if (pictureHeight / screenHeight > pictureWidth / screenWidth) {
				pixelToPixelRatio = pictureHeight / screenHeight;
			} else {
				pixelToPixelRatio = pictureWidth / screenWidth;
			}

		}

		/**
		 * This method will scale the picture to the same scale as preview,
		 * given that the imageview is the same size as the picture.
		 * 
		 * @param params
		 *            the parameters of the camera
		 */

		public void setSameScale(Parameters params) {
			Size previewSize = params.getPreviewSize();
			float previewHeight = previewSize.height * Setting.BASIC_SCALE;
			float previewWidth = previewSize.width * Setting.BASIC_SCALE;
			float pictureHeight = mSnapShotView.getDrawable()
					.getIntrinsicHeight();
			float pictureWidth = mSnapShotView.getDrawable()
					.getIntrinsicWidth();
			if (pictureHeight / previewHeight > pictureWidth / previewWidth) {
				sameScaleRatio = previewWidth / pictureWidth;
			} else {
				sameScaleRatio = previewHeight / pictureHeight;
			}

		}

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {

		}

		@Override
		public void onProgressChanged(SeekBar seekBar, int progress,
				boolean fromUser) {
			Log.d("Zoom bar", "onProgressChanged:" + progress);
			Log.d("Zoom bar", "Image Locked?" + isImageLocked);
			if (!isImageLocked) {
				mCameraPreview.setBestZoomAndScale((float) progress
						/ (float) seekBar.getMax());
			} else {
				// if (Compatibility.isGalaxyS3()) {
				//
				// mCameraPreview.setScaleForSIII(progress, seekBar.getMax());
				// return;
				// }
				float minScale = 1;
				float maxScale = Setting.MAX_SCALE_FACTOR;
				// float zoomScale =
				// (Setting.MAX_ZOOMVALUE/10.0f+1)/(mCameraZoom/10.0f+1);
				// viewPreviewRatio=1;
				float zoomScale = 1;
				float scale = (minScale + (float) (progress) / seekBar.getMax()
						* (maxScale - minScale) * zoomScale)
						* pixelToPixelRatio * sameScaleRatio;
				mSnapShotView.setScaleX(scale);
				mSnapShotView.setScaleY(scale);
				Log.d("Zoom bar", "onProgressChanged set scale:" + scale);
				// Log.d("Pinch_Zoom", "Progress:"+progress+" Scale:"+scale);
				restrictInsideBoundary(mSnapShotView);
				Log.d("RestoreState", "Set Scale");

			}

			//
		}
	};

	@Override
	protected void onDestroy() {
		mSocialController.onDestroy(this);
		super.onDestroy();
		if (mCameraPreview != null)
			mCameraPreview.disableView();
	}

	/**
	 * Restricts the SnapshotView so that it would not go out of boundary.
	 * 
	 * @param view
	 *            The view that is to be restricted.
	 */
	protected void restrictInsideBoundary(ImageView view) {
		if (!Setting.ALLOW_OUT_OF_BOUNDARY) {

			Display display = getWindowManager().getDefaultDisplay();

			float boundaryWidth = (view.getWidth() - display.getWidth()
					/ view.getScaleX()) / 2;

			float boundaryHeight = (view.getHeight() - display.getHeight()
					/ view.getScaleY()) / 2;

			int x = view.getScrollX();
			int y = view.getScrollY();

			/*
			 * boundaryWidth and boundaryHeight > 0 means the SnapshotView is
			 * larger than the screen.
			 */
			if (boundaryWidth > 0) {
				if (x > boundaryWidth) {
					x = (int) boundaryWidth;
				} else if (x < -1 * boundaryWidth) {
					x = (int) (-1 * boundaryWidth);
				}
			} else {
				if (x < boundaryWidth) {
					x = (int) boundaryWidth;
				} else if (x > -1 * boundaryWidth) {
					x = (int) (-1 * boundaryWidth);
				}
			}
			if (boundaryHeight > 0) {
				if (y > boundaryHeight) {
					y = (int) boundaryHeight;
				} else if (y < -1 * boundaryHeight) {
					y = (int) (boundaryHeight * -1);
				}
			} else {
				if (y < boundaryHeight) {
					y = (int) boundaryHeight;
				} else if (y > -1 * boundaryHeight) {
					y = (int) (boundaryHeight * -1);
				}
			}

			view.scrollTo(x, y);

		}
	}

	/**
	 * Save the current state of the activity.
	 */
	private void saveState() {
		if (mCameraPreview != null) {
			if (mCameraPreview.getCamera() != null) {
				mCameraZoom = mCameraPreview.getCamera().getParameters()
						.getZoom();
			}
		}
		mProgress = mZoombar.getProgress();
	}

	/**
	 * restore the state saved before.
	 */
	private void restoreState() {
		
		Log.d("RestoreState", "Restore state!");
		if (mCameraPreview.getCamera() != null) {
			Log.d("RestoreState", "Get Camera!");
			/*
			 * if(mCameraZoom>=0){ mCameraPreview.setZoom(mCameraZoom);
			 * 
			 * } if(mScale>0){ mCameraPreview.setScale(mScale); }
			 */
			mZoombar.setProgress(mProgress);
			mZoomListener.onProgressChanged(mZoombar, mProgress, false);
			// mZoombar.setProgress(mProgress);

			Log.d("LockState", "set FlashLight:" + isFlashOn);
			// if (isFlashOn) {
			// mFlashBtn.setBackground(getResources().getDrawable(
			// R.drawable.flashon));
			// UMENGhelper.onFlashLightBegin(this);
			// }
			Log.d("LockState", "isLocked??" + isImageLocked);
			if (isImageLocked) {

			}

			mCameraPreview.setHorizontalLock(isHorizontalLocked);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		/*
		 * Here is a bug of Umeng framework. If two events are recorded at the
		 * same time, then probably one of the event would be lost. Otherwise if
		 * they are seperated by a certain amount of time, these two events will
		 * be recorded correctly.
		 */
		UMENGhelper.onLockscreenEnd(MainActivity.this);
		if (!isImageLocked) {
			UMENGhelper.onUserExit(MainActivity.this,
					mCameraPreview.getMagnification());

		}
		/*
		 * try { Thread.sleep(500); } catch (InterruptedException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 */

		UMENGhelper.onFlashLightEnd(MainActivity.this);
		mCameraPreview.setFlashLight(false);
		isFlashOn = false;
		mFlashBtn
				.setBackground(getResources().getDrawable(R.drawable.flashoff));

		saveState();
		myOrientationEventListener.disable();

		mCameraPreview.disableView();
		/* All Umeng call should be before MobclickAgent.onPause(); */
		UMENGhelper.onPause(this);
		
		mSocialController.onPause(this);

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN
				|| keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
			
			handleImageLock();
			return true;
		}
		if (keyCode == KeyEvent.KEYCODE_BACK) {

			//AppRater.resetAllValues(prefs.edit());
			
			
			startActivity(new Intent(Intent.ACTION_MAIN)
					.addCategory(Intent.CATEGORY_HOME));
//			openOptionsMenu();
			
			
		}
		
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onResume() {

		super.onResume();
		TestinAgent.onResume(this);
		loadPreference();
		/* All Umeng calls should be after MobclickAgent.onResume(); */
		UMENGhelper.onResume(this);
		
		if (mSnapShotView.getVisibility() == View.VISIBLE) {
			UMENGhelper.onLockscreenBegin(this);

		}
		myOrientationEventListener.enable();

		/* Load the OpenCV library statically. */
		if (!OpenCVLoader.initDebug()) {

		} else {
			mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);

		}

		mSocialController.onResume(this);

	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		// TODO Auto-generated method stub
		super.onWindowFocusChanged(hasFocus);

	}

	/**
	 * Listens to the orientation change of the phone and redos the layout.
	 * There are bugs in this method with tablets whose orientations are
	 * ORIENTATION_DOWN when in landscape mode.
	 * 
	 * @author Huaqi Yi
	 * 
	 */
	private class MyOrientationEventListener extends OrientationEventListener {
		Context context;

		public MyOrientationEventListener(Context context) {
			super(context);
			this.context = context;
		}

		@Override
		public void onOrientationChanged(int orientation) {
			int halfrange = 45;
			if (orientation == ORIENTATION_UNKNOWN)
				return;

			int mode = 0;
			if (orientation <= 0 + halfrange || orientation > 360 - halfrange) {
				mode = ORIENTATION_DOWN;
			} else if (orientation > 90 - halfrange
					&& orientation <= 90 + halfrange) {
				mode = ORIENTATION_RIGHT;
			} else if (orientation > 180 - halfrange
					&& orientation <= 180 + halfrange) {
				mode = ORIENTATION_UP;
			} else if (orientation > 270 - halfrange
					&& orientation <= 270 + halfrange) {
				mode = ORIENTATION_LEFT;
			} else {
				mode = mOrientationMode;
			}
			/* Redo layout only when the orientation changed. */
			if (mode != mOrientationMode) {
				mOrientationMode = mode;
				mCameraPreview.setOrientationMode(mOrientationMode);
				rearrangeLayout(mOrientationMode);
			}

		}

		/**
		 * Rearranges layout of all widgets based on the given orientation mode.
		 * 
		 * @param mOrientationMode
		 *            orientation of the device
		 */

		private void rearrangeLayout(int mOrientationMode) {
			mWidgetsLayout.removeAllViews();

			rearrangeZoomBar(mOrientationMode);
			rearrangeButtons(mOrientationMode);

		}

		/**
		 * Rearranges the buttons based on the given orientation of the device.
		 * 
		 * @param mOrientationMode
		 *            orientation of the device.
		 */
		private void rearrangeButtons(int mOrientationMode) {
			int buttonParentBottom = RelativeLayout.ALIGN_PARENT_RIGHT;
			int buttonParentLeft = RelativeLayout.ALIGN_PARENT_BOTTOM;
			int buttonParentRight = RelativeLayout.ALIGN_PARENT_TOP;
			int buttonToLeft = RelativeLayout.BELOW;
			int buttonSize = Setting.BUTTON_SIZE;
			int rotation = 0;
			int marginLeft = 0;
			int marginRight = 0;
			int marginTop = 0;
			int marginBottom = 0;
			float density = context.getResources().getDisplayMetrics().density;
			int buttonSizeDp = (int) (buttonSize * density);

			switch (mOrientationMode) {
			case ORIENTATION_DOWN:
				buttonParentBottom = RelativeLayout.ALIGN_PARENT_RIGHT;
				buttonParentLeft = RelativeLayout.ALIGN_PARENT_BOTTOM;
				buttonToLeft = RelativeLayout.BELOW;
				buttonParentRight = RelativeLayout.ALIGN_PARENT_TOP;
				rotation = 0;
				marginTop = Setting.BUTTON_MARGIN;
				break;
			case ORIENTATION_LEFT:
				buttonParentBottom = RelativeLayout.ALIGN_PARENT_BOTTOM;
				buttonParentLeft = RelativeLayout.ALIGN_PARENT_LEFT;
				buttonToLeft = RelativeLayout.LEFT_OF;
				buttonParentRight = RelativeLayout.ALIGN_PARENT_RIGHT;
				rotation = 90;
				marginRight = Setting.BUTTON_MARGIN;

				break;
			case ORIENTATION_RIGHT:
				buttonParentBottom = RelativeLayout.ALIGN_PARENT_TOP;
				buttonParentLeft = RelativeLayout.ALIGN_PARENT_RIGHT;
				buttonToLeft = RelativeLayout.RIGHT_OF;
				buttonParentRight = RelativeLayout.ALIGN_PARENT_LEFT;
				rotation = 270;
				marginLeft = Setting.BUTTON_MARGIN;
				break;
			case ORIENTATION_UP:
				buttonParentBottom = RelativeLayout.ALIGN_PARENT_LEFT;
				buttonParentLeft = RelativeLayout.ALIGN_PARENT_TOP;
				buttonToLeft = RelativeLayout.ABOVE;
				buttonParentRight = RelativeLayout.ALIGN_PARENT_BOTTOM;
				rotation = 180;
				marginBottom = Setting.BUTTON_MARGIN;
				break;
			default:
				break;
			}

			LayoutParams mHorizontalLockBtnParams = new LayoutParams(
					buttonSizeDp, buttonSizeDp);

			LayoutParams mFlashLightBtnParams = new LayoutParams(buttonSizeDp,
					buttonSizeDp);
			mFlashLightBtnParams.setMargins(marginLeft, marginTop, marginRight,
					marginBottom);
			LayoutParams mSnapshotBtnParams = new LayoutParams(buttonSizeDp,
					buttonSizeDp);
			mSnapshotBtnParams.setMargins(marginLeft, marginTop, marginRight,
					marginBottom);
			LayoutParams mInfoParams = new LayoutParams(buttonSizeDp,
					buttonSizeDp);

			mHorizontalLockBtnParams.addRule(buttonParentBottom);
			mHorizontalLockBtnParams.addRule(buttonParentLeft);

			mWidgetsLayout
					.addView(mHorizontalLockBtn, mHorizontalLockBtnParams);
			mWidgetsLayout.addView(mSharePictureBtn, mHorizontalLockBtnParams);
			mHorizontalLockBtn.setRotation(rotation);
			mSharePictureBtn.setRotation(rotation);

			mInfoParams.addRule(buttonParentBottom);
			mInfoParams.addRule(buttonParentRight);
			mWidgetsLayout.addView(mInfoBtn, mInfoParams);
			mInfoBtn.setRotation(rotation);

			mSnapshotBtnParams.addRule(buttonParentBottom);
			mSnapshotBtnParams.addRule(buttonToLeft, mInfoBtn.getId());
			mWidgetsLayout.addView(mSnapshotBtn, mSnapshotBtnParams);
			mSnapshotBtn.setRotation(rotation);

			mFlashLightBtnParams.addRule(buttonParentBottom);
			mFlashLightBtnParams.addRule(buttonToLeft, mSnapshotBtn.getId());
			mWidgetsLayout.addView(mFlashBtn, mFlashLightBtnParams);
			mWidgetsLayout.addView(mSavePictureBtn, mFlashLightBtnParams);
			mFlashBtn.setRotation(rotation);
			mSavePictureBtn.setRotation(rotation);

		}

		/**
		 * Rearranges the zoombar based on the given orientation mode of the
		 * device.
		 * 
		 * @param mOrientationMode
		 *            orientation of the device.
		 */
		// TODO: When rotated 90 or 270 degrees, the seekbar has some issues
		// on its position towards parent layout.
		private void rearrangeZoomBar(int mOrientationMode) {
			Display mDisplay = ((WindowManager) context
					.getSystemService(Context.WINDOW_SERVICE))
					.getDefaultDisplay();
			int screenHeight = mDisplay.getWidth();
			int screenWidth = mDisplay.getHeight();

			int zoomBarWidth = screenHeight / 2;
			int zoomBarHeight = LayoutParams.WRAP_CONTENT;
			int zoomBarRuleParentRight = RelativeLayout.ALIGN_PARENT_TOP;
			int zoomBarRuleMiddle = RelativeLayout.CENTER_HORIZONTAL;
			int zoomBarRotation = 180;
			int zoomBarpaddingPixel = 30;
			float density = context.getResources().getDisplayMetrics().density;
			int zoomBarpaddingTopDp = 0;
			int zoomBarpaddingBottomDp = 0;
			int zoomBarpaddingLeftDp = (int) (zoomBarpaddingPixel * density);
			int zoomBarpaddingRightDp = (int) (zoomBarpaddingPixel * density);
			switch (mOrientationMode) {
			case ORIENTATION_DOWN:
				zoomBarWidth = screenHeight / 2;
				zoomBarHeight = LayoutParams.WRAP_CONTENT;
				zoomBarRuleParentRight = RelativeLayout.ALIGN_PARENT_TOP;
				zoomBarRuleMiddle = RelativeLayout.CENTER_HORIZONTAL;
				zoomBarRotation = 180;
				zoomBarpaddingPixel = 30;
				density = context.getResources().getDisplayMetrics().density;
				zoomBarpaddingTopDp = 0;
				zoomBarpaddingBottomDp = 0;
				zoomBarpaddingLeftDp = (int) (zoomBarpaddingPixel * density);
				zoomBarpaddingRightDp = (int) (zoomBarpaddingPixel * density);
				break;
			case ORIENTATION_RIGHT:
				zoomBarWidth = screenWidth / 2;
				zoomBarHeight = LayoutParams.WRAP_CONTENT;
				zoomBarRuleParentRight = RelativeLayout.ALIGN_PARENT_LEFT;
				zoomBarRuleMiddle = RelativeLayout.CENTER_VERTICAL;
				zoomBarRotation = 90;
				zoomBarpaddingPixel = 0;
				density = context.getResources().getDisplayMetrics().density;
				zoomBarpaddingTopDp = (int) (zoomBarpaddingPixel * density);
				zoomBarpaddingBottomDp = (int) (zoomBarpaddingPixel * density);
				zoomBarpaddingLeftDp = 0;
				zoomBarpaddingRightDp = 0;

				break;
			case ORIENTATION_UP:
				zoomBarWidth = screenHeight / 2;
				zoomBarHeight = LayoutParams.WRAP_CONTENT;
				zoomBarRuleParentRight = RelativeLayout.ALIGN_PARENT_BOTTOM;
				zoomBarRuleMiddle = RelativeLayout.CENTER_HORIZONTAL;
				zoomBarRotation = 0;
				zoomBarpaddingPixel = 30;
				density = context.getResources().getDisplayMetrics().density;
				zoomBarpaddingTopDp = 0;
				zoomBarpaddingBottomDp = 0;
				zoomBarpaddingLeftDp = (int) (zoomBarpaddingPixel * density);
				zoomBarpaddingRightDp = (int) (zoomBarpaddingPixel * density);
				break;
			case ORIENTATION_LEFT:
				zoomBarWidth = screenWidth / 2;
				zoomBarHeight = LayoutParams.WRAP_CONTENT;
				zoomBarRuleParentRight = RelativeLayout.ALIGN_PARENT_RIGHT;
				zoomBarRuleMiddle = RelativeLayout.CENTER_VERTICAL;
				zoomBarRotation = 270;
				zoomBarpaddingPixel = 0;
				density = context.getResources().getDisplayMetrics().density;
				zoomBarpaddingTopDp = (int) (zoomBarpaddingPixel * density);
				zoomBarpaddingBottomDp = (int) (zoomBarpaddingPixel * density);
				zoomBarpaddingLeftDp = 0;
				zoomBarpaddingRightDp = 0;

				break;
			default:
				break;
			}

			LayoutParams mZoombarParams = new LayoutParams(zoomBarWidth,
					zoomBarHeight);

			mZoombarParams.addRule(zoomBarRuleMiddle);
			mZoombarParams.addRule(zoomBarRuleParentRight);

			mWidgetsLayout.addView(mZoombar, mZoombarParams);
			mZoombar.setRotation(zoomBarRotation);
			mZoombar.setPadding(zoomBarpaddingLeftDp, zoomBarpaddingTopDp,
					zoomBarpaddingRightDp, zoomBarpaddingBottomDp);
			mWidgetsLayout.requestTransparentRegion(mZoombar);

		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		/* Inflate the menu; this adds items to the action bar if it is present. */
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		if (item.getItemId() == R.id.volunteer) {
			startActivity(new Intent(MainActivity.this,
					AuthorizationActivity.class));
		} else if (item.getItemId() == R.id.previewsize) {
			startActivity(new Intent(MainActivity.this,
					SettingPreviewActivity.class));

		}

		return false;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);

	}

	@Override
	protected void onStop() {
		Log.d("onStopHappen", "onStop");
		super.onStop();
		TestinAgent.onStop(this);
		mSocialController.onStop(this);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		// Social.onSaveInstanceState(outState);
	}

	/**
	 * Get the cached image displayed on SnapshotView or CameraPreview (for
	 * Galaxy SIII) Should only be called when the screen is locked.
	 * 
	 * @return
	 */
	private Bitmap getCachedImage() {
		if (Compatibility.isGalaxyS3()) {
			mCameraPreview.setDrawingCacheEnabled(true);
			return mCameraPreview.getDrawingCache()
					.copy(Config.ARGB_8888, true);
		} else {
			// saveBitmap = mSnapShot;
			// mSnapShotView.setDrawingCacheEnabled(true);
			// return mSnapShotView.getDrawingCache();
			return mGalleryManager.getCurrentBitmap();
		}

	}

	/**
	 * Save the CachedImage in SnapshotView into local disk.
	 */
	private void savePicture() {
		mGalleryManager.SavePicture();
		// SavePictureTask savePictureTask = new SavePictureTask();
		// savePictureTask.execute();

	}

	/**
	 * Lock the screen by taking a picture.
	 */
	private void executeTakePictureTask() {
		/* Take picture in another thread to avoid blocking the UI thread. */
		if (Compatibility.isGalaxyS3()) {
			UMENGhelper.onUserFocus(MainActivity.this, mCameraPreview
					.getOpiticalFocusDistance(mCameraPreview.getCamera()));
			takePicture(mCameraPreview.getCamera());

		} else {
			TakePictureTask takePictureTask = new TakePictureTask();
			takePictureTask.execute();
		}
	}

	/**
	 * AsyncTask that takes a picture, and displays it on SnapshotView.
	 * 
	 * @author Huaqi Yi
	 * 
	 */
	class TakePictureTask extends AsyncTask<Void, Void, Void> {
		Camera mCamera = mCameraPreview.getCamera();

		@Override
		protected Void doInBackground(Void... p) {

			if (Setting.focusBeforeTakePicture) {
				if (mCamera != null) {
					mCamera.cancelAutoFocus();
//					android.graphics.Rect focusRect = new Rect(-1000, -1000,
//							1000, 1000);
//					android.graphics.Rect meteringRect = new Rect(-1000, -1000,
//							1000, 1000);
//					Camera.Parameters params = mCamera.getParameters();
//					params.setFocusMode(Parameters.FOCUS_MODE_AUTO);
//					ArrayList<Camera.Area> focusAreas = new ArrayList<Camera.Area>();
//					ArrayList<Camera.Area> meteringAreas = new ArrayList<Camera.Area>();
//					focusAreas.add(new Area(focusRect, 1000));
//					meteringAreas.add(new Area(meteringRect, 1000));
//					params.setFocusAreas(focusAreas);
//					params.setMeteringAreas(meteringAreas);
//					mCamera.setParameters(params);
					mCamera.autoFocus(new AutoFocusCallback() {

						@Override
						public void onAutoFocus(boolean success, Camera camera) {
							if (success) {
								UMENGhelper
										.onUserFocus(
												MainActivity.this,
												mCameraPreview
														.getOpiticalFocusDistance(mCameraPreview
																.getCamera()));
							} else {
								Log.d("AutoFocus", "Focus Fail");
							}
							Log.d("AutoFocus", "Autofocus finish!");
							// camera.takePicture(null, null, mPictureCallback);
							takePicture(mCamera);

						}
					});

				}
			} else {
				UMENGhelper.onUserFocus(MainActivity.this, mCameraPreview
						.getOpiticalFocusDistance(mCameraPreview.getCamera()));
				takePicture(mCamera);
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {

		}

	}

	/**
	 * Take the picture and display it in imageview
	 * @param mCamera
	 */
	public void takePicture(Camera mCamera) {
		if (!Compatibility.isGalaxyS3()) {
			mCamera.takePicture(null, null, mPictureCallback);
		} else {
			Bitmap mSnapShot = getCachedImage();
			mCamera.stopPreview();
			displaySnapshot(mCamera, mSnapShot);
			mZoomListener.onProgressChanged(mZoombar, mZoombar.getProgress(),
					false);

			showSaveAndShareButton();
		}
	}

	/**
	 * This callback will display the picture on SnapshoView and handle the
	 * seekbar.
	 */
	private PictureCallback mPictureCallback = new PictureCallback() {

		@Override
		public void onPictureTaken(byte[] data, Camera camera) {
			Log.d("OutOfMemoryLog", "---------------------Before Decoding Picture-------------------------");
			Log.d("OutOfMemoryLog", MemoryUsage.getMemoryUsage());
			
			Bitmap mSnapShot = BitmapFactory.decodeByteArray(data, 0, data.length);
			Log.d("OutOfMemoryLog", "---------------------After Decoding Picture-------------------------");
			Log.d("OutOfMemoryLog", MemoryUsage.getMemoryUsage());
			Log.d("OutOfMemoryLog", "Decoded Picture Size:"+(NumberFormat.getInstance().format(mSnapShot.getByteCount()/1024)));
			
			if (mSnapShot != null) {

				displaySnapshot(camera,mSnapShot);
				showSaveAndShareButton();

			} else {
				throw new RuntimeException(
						"The image taken cannot be decoded. Phone Model:"
								+ Build.MODEL);
			}

			/*
			 * Just for test purpose of the Galaxy S3.
			 * if(Setting.testForTa‘、 kingPicture){ File dir = new
			 * File(Environment.
			 * getExternalStorageDirectory().getAbsolutePath()+
			 * "/test_screenshot" ); dir.mkdirs(); File file = new
			 * File(dir,"test.png"); try{ FileOutputStream fos = new
			 * FileOutputStream(file);
			 * mSnapshot.compress(Bitmap.CompressFormat.PNG, 100, fos);
			 * fos.flush(); fos.close(); } catch (Exception e) {
			 * 
			 * } }
			 */
		}

	};

	/**
	 * Display the save and share button.
	 */
	public void showSaveAndShareButton() {
		mSavePictureBtn.setVisibility(View.VISIBLE);

		if (Social.show_share_btn) {
			mSharePictureBtn.setVisibility(View.VISIBLE);
		} else {
			mSharePictureBtn.setVisibility(View.GONE);
		}
		mSnapshotBtn.setVisibility(View.VISIBLE);
		mSnapshotBtn.setClickable(true);
		isHandlingImageLock = false;
	}

	/**
	 * Make the preview invisible and display the given Bitmap in imageview 
	 * @param camera
	 * @param mSnapShot
	 */
	public void displaySnapshot(Camera camera,Bitmap mSnapShot) {

		Parameters parameters = camera.getParameters();
		mSnapShotView.setImageBitmap(mSnapShot);

		mSnapShotView.setVisibility(View.VISIBLE);
		previewSize = camera.getParameters().getPreviewSize();
		int pictureHeight = mSnapShot.getHeight();
		int pictureWidth = mSnapShot.getWidth();
		// Log.d("Scale_Picture", "Image Size:"+
		// pictureWidth+":"+pictureHeight);
		// Log.d("Scale_Picture", "Picture Size:"+
		// parameters.getPictureSize().width+":"+parameters.getPictureSize().height);
		// Log.d("Scale_Picture",
		// "Preview:"+previewSize.width+":"+previewSize.height);
		// Log.d("Scale_Picture",
		// "CameraPreview:"+mCameraPreview.getWidth()+":"+mCameraPreview.getHeight());
		// Log.d("Scale_Picture",
		// "SnapShotView:"+mSnapShotView.getWidth()+":"+mSnapShotView.getHeight());
		float viewPreviewRatio = mCameraPreview.getWidth()
				/ (float) (previewSize.width);
		if ((float) (pictureWidth) / (float) pictureHeight > (float) mCameraPreview
				.getWidth() / (float) mCameraPreview.getHeight()) {
			// Probably only the width of camera data is cropped.
			viewPreviewRatio = (float) pictureHeight
					/ (float) mCameraPreview.getHeight();
		} else {
			// Probably only the height of the camera data is cropped.
			viewPreviewRatio = (float) pictureWidth
					/ (float) mCameraPreview.getWidth();
		}

		float scale = mCameraPreview.getScale() / viewPreviewRatio;

		mCameraPreview.disableView();
		mCameraPreview.releaseFrozonMat();
		UpdateZoombarOnLocked();

		/* Adjust the SnapshotView to the same size of the preview. */
		mZoomListener.setPixelToPixelRatio(parameters);
		mZoomListener.setSameScale(parameters);
		mZoomListener
				.onProgressChanged(mZoombar, mZoombar.getProgress(), false);

		mCameraPreview.setVisibility(View.INVISIBLE);
		mSnapShotView.scrollTo(0, 0);
		mGalleryManager.setSnapshotBitmap(mSnapShot, mOrientationMode);
		mGalleryManager.updateFiles();
		mGalleryManager.resetIndex();

	}

}
