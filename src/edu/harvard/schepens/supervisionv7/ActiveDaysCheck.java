package edu.harvard.schepens.supervisionv7;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.format.Time;
import android.util.Log;
import android.widget.Toast;

public class ActiveDaysCheck {
	/**
	 * Saves the date that user installed the app first time.
	 * And counts the number of active day of user using this app.
	 * @param prefs
	 */
	public static void checkActiveValue(SharedPreferences prefs,Context context){
		
		Time time = new Time();
		
		Time installedTime = new Time();
		Time lastActiveTime = new Time();
		time.setToNow();
		// This line is critical, otherwise it would always be the day after lastActiveTime
		time.parse3339(time.format3339(true));
		SharedPreferences.Editor editor = prefs.edit();
		String installedTimeString = prefs.getString(Setting.PREFERENCE_INSTALLED_DATE, "");
		String lastActiveTimeString = prefs.getString(Setting.PREFERENCE_LAST_ACTIVE_DATE, "");
		int totalActiveDays = prefs.getInt(Setting.PREFERENCE_TOTAL_ACTIVE_DAYS, 0);
		if(installedTimeString.isEmpty()){
			if(Setting.displayToastForDebugging){
				Toast.makeText(context, "Installed on "+time.format3339(true), Toast.LENGTH_LONG).show();
			}
			
			editor.putString(Setting.PREFERENCE_INSTALLED_DATE, time.format3339(true));
		} else {
			installedTime.parse3339(installedTimeString);
			if(Setting.displayToastForDebugging){
				Toast.makeText(context, "Installed on "+installedTime.format3339(true), Toast.LENGTH_LONG).show();
				Toast.makeText(context, "Installed for "+julianDayDifference(time, installedTime)+" Days", Toast.LENGTH_LONG).show();
			}
		}
		
		if(lastActiveTimeString.isEmpty()){
			editor.putString(Setting.PREFERENCE_LAST_ACTIVE_DATE, time.format3339(true));
			totalActiveDays++;
		} else {
			lastActiveTime.parse3339(lastActiveTimeString);
			if(julianDayDifference(time, lastActiveTime)==0){
			Log.d("ActiveDays", "Same date");	
			} else {
				Log.d("ActiveDays", "Time String:"+time.format3339(true)+"   Last Active:"+lastActiveTimeString);
				Log.d("ActiveDays", "Time:"+time.format3339(true)+"   Last Active:"+lastActiveTime.format3339(true));
				Log.d("ActiveDays", "Day difference:"+julianDayDifference(time, lastActiveTime));
				totalActiveDays++;
				editor.putInt(Setting.PREFERENCE_TOTAL_ACTIVE_DAYS, totalActiveDays);
				editor.putString(Setting.PREFERENCE_LAST_ACTIVE_DATE, time.format3339(true));
			}
		}
		
		if(Setting.displayToastForDebugging){
			Toast.makeText(context, "Active for "+totalActiveDays+" Days", Toast.LENGTH_LONG).show();
		}
		editor.commit();
	}
	
	/**
	 * Given two time, calculate the day difference between them
	 * @param first the later time
	 * @param second the earlier time  
	 * @return day difference between the later time and earlier time.
	 */
	private static int julianDayDifference(Time first,Time second){
		int firstJulian = Time.getJulianDay(first.toMillis(true),0);
		int secondJulian = Time.getJulianDay(second.toMillis(true), 0);
		Log.d("ActiveDays","First Julian Day:"+firstJulian);
		Log.d("ActiveDays","Second Julian Day:"+secondJulian);
		return Time.getJulianDay(first.toMillis(true),0) - Time.getJulianDay(second.toMillis(true), 0);
	}
	
	public static int getInstalledDays(SharedPreferences prefs){
		Time time = new Time();
		Time installedTime = new Time();
		time.setToNow();
		time.parse3339(time.format3339(true));
		String installedTimeString = prefs.getString(Setting.PREFERENCE_INSTALLED_DATE, "");
		installedTime.parse3339(installedTimeString);
		return julianDayDifference(time, installedTime);
	}
	
	public static int getActiveDays(SharedPreferences prefs){
		int totalActiveDays = prefs.getInt(Setting.PREFERENCE_TOTAL_ACTIVE_DAYS, 0);
		return totalActiveDays;
	}
}
