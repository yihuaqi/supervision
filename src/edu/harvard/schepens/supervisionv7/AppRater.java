package edu.harvard.schepens.supervisionv7;

import java.util.Locale;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AppRater {

    private final static String APP_TITLE = "SuperVision+ Magnifier";
    private final static String APP_PNAME = "edu.harvard.schepens.supervisionv7";
    
    private final static String TAG = "AppRater";
    private static final String OldGooglePlayStorePackageName = "com.google.market";
    private static final String NewGooglePlayStorePackageName = "com.android.vending";
    
    private final static int INSTALL_DAYS_UNTIL_PROMPT = 14;
    private final static int ACTIVE_DAYS_UNTIL_PROMPT = 3;
    
    private final static String RATE_APP_VERSION_KEY = "appversion";
    private final static String NOT_RATE_FOR_THIS_VERSION = "currentversion";
    private final static String NOT_RATE_FOR_ALL_VERSION = "allversion";
    
    private  static String RATE_DIALOG_TITLE = "Rate SuperVision+ Magnifier";
    private final static String RATE_DIALOG_TITLE_EN = "Rate SuperVision+ Magnifier";
    private final static String RATE_DIALOG_TITLE_SC = "给新视力评分";
    private final static String RATE_DIALOG_TITLE_TC = "給新視力評分";
    
    
    private  static String RATE_DIALOG_TEXT = "Would you like to give our app a 5-star rating?";
    private final static String RATE_DIALOG_TEXT_EN = "Would you like to give our app a 5-star rating?";
    private final static String RATE_DIALOG_TEXT_SC = "你愿意给我们的app一个5颗星的好评吗?";
    private final static String RATE_DIALOG_TEXT_TC = "你願意給我們的app一個5顆星的好評嗎?";
    
    private  static String RATE_BTN_TEXT = "Rate";
    private final static String RATE_BTN_TEXT_EN = "Rate";
    private final static String RATE_BTN_TEXT_SC = "好评";
    private final static String RATE_BTN_TEXT_TC = "好評";
    
    private  static String NOT_NOW_BTN_TEXT = "Not Now";
    private final static String NOT_NOW_BTN_TEXT_EN = "Not Now";
    private final static String NOT_NOW_BTN_TEXT_SC = "以后再说";
    private final static String NOT_NOW_BTN_TEXT_TC = "以后再说";
    
    private  static String NO_WAY_BTN_TEXT = "No Way";
    private final static String NO_WAY_BTN_TEXT_EN = "No Way";
    private final static String NO_WAY_BTN_TEXT_SC = "残忍的拒绝";
    private final static String NO_WAY_BTN_TEXT_TC = "殘忍的拒絕";
    
    public static void app_launched(Activity mContext) {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(mContext);
		String lang = Locale.getDefault().getLanguage();
		Log.d("Localization","locale:"+Locale.getDefault().toString());
		Log.d("Localization","locale:"+Locale.TRADITIONAL_CHINESE);
		Log.d("Localization","locale:"+Locale.SIMPLIFIED_CHINESE);
		if(Locale.getDefault().equals(Locale.SIMPLIFIED_CHINESE)){
			Log.d("Localization","locale:"+Locale.getDefault().toString());
			RATE_DIALOG_TITLE = RATE_DIALOG_TITLE_SC;
			RATE_DIALOG_TEXT = RATE_DIALOG_TEXT_SC;
			RATE_BTN_TEXT = RATE_BTN_TEXT_SC;
			NOT_NOW_BTN_TEXT = NOT_NOW_BTN_TEXT_SC;
			NO_WAY_BTN_TEXT = NO_WAY_BTN_TEXT_SC;
		} else if(Locale.getDefault().equals(Locale.TRADITIONAL_CHINESE)){
			Log.d("Localization","locale:"+Locale.getDefault().toString());
			RATE_DIALOG_TITLE = RATE_DIALOG_TITLE_TC;
			RATE_DIALOG_TEXT = RATE_DIALOG_TEXT_TC;
			RATE_BTN_TEXT = RATE_BTN_TEXT_TC;
			NOT_NOW_BTN_TEXT = NOT_NOW_BTN_TEXT_TC;
			NO_WAY_BTN_TEXT = NO_WAY_BTN_TEXT_TC;
		
		} else {
			RATE_DIALOG_TITLE = RATE_DIALOG_TITLE_EN;
			RATE_DIALOG_TEXT = RATE_DIALOG_TEXT_EN;
			RATE_BTN_TEXT = RATE_BTN_TEXT_EN;
			NOT_NOW_BTN_TEXT = NOT_NOW_BTN_TEXT_EN;
			NO_WAY_BTN_TEXT = NO_WAY_BTN_TEXT_EN;
		}
		
    	//SharedPreferences prefs = mContext.getSharedPreferences(name, mode);
    	if(shouldRate(mContext)){
		   SharedPreferences.Editor editor = prefs.edit();
   	        
   	        int active_days = ActiveDaysCheck.getActiveDays(prefs);
   	        int installedDays = ActiveDaysCheck.getInstalledDays(prefs);
   	        // 
   	        
   	        // Wait at least n days before opening
   	        if (active_days >= ACTIVE_DAYS_UNTIL_PROMPT && installedDays >= INSTALL_DAYS_UNTIL_PROMPT) {
   	        	Log.d(TAG, "Active:"+active_days+" installed:"+installedDays);
   	            showRateDialog(mContext, getVersionCode(mContext));
   	        }
   	        
   	        editor.commit();
    	}
	        
	     

        
    }   
    
    public static void showRateDialog(final Activity mContext, final int currentVersionCode) {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(mContext);
		final SharedPreferences.Editor editor = prefs.edit();
        final Dialog dialog = new Dialog(mContext);
        dialog.setTitle(RATE_DIALOG_TITLE);
        

        LinearLayout ll = new LinearLayout(mContext);
        ll.setOrientation(LinearLayout.VERTICAL);
        
        TextView tv = new TextView(mContext);
        tv.setText(RATE_DIALOG_TEXT);
        tv.setWidth(800);
        tv.setPadding(4, 0, 4, 10);
        ll.addView(tv);
        
        Button b1 = new Button(mContext);
        b1.setText(RATE_BTN_TEXT);
        b1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				if(checkPlayStoreInstalled(mContext)){
					Log.d(TAG, "Rate using play store!");
					mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + mContext.getPackageName())));
				} else {
					Log.d(TAG, "Rate using browser!");
					mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=edu.harvard.schepens.supervisionv7&hl=en")));
				}
                if (editor != null) {
                    //editor.putBoolean("dontshowagain", true);
                    //editor.commit();
                	editor.putBoolean(NOT_RATE_FOR_ALL_VERSION, true);
                	editor.commit();
                	Log.d(TAG, "Rated!");
                }
                dialog.dismiss();
				
			}
        });        
        ll.addView(b1);

        Button b2 = new Button(mContext);
        b2.setText(NOT_NOW_BTN_TEXT);
        b2.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	Log.d(TAG, "Not now!");
            	if(editor!=null){
            		editor.putInt(RATE_APP_VERSION_KEY,currentVersionCode);
                	editor.putBoolean(NOT_RATE_FOR_THIS_VERSION, true);
                	editor.commit();
                	Log.d(TAG, "Not now for "+currentVersionCode);
            	}
            	
                dialog.dismiss();
            }
        });
        ll.addView(b2);

        Button b3 = new Button(mContext);
        b3.setText(NO_WAY_BTN_TEXT);
        b3.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (editor != null) {
                    //editor.putBoolean("dontshowagain", true);
                    //editor.commit();
                	editor.putBoolean(NOT_RATE_FOR_ALL_VERSION, true);
                	editor.commit();
                	Log.d(TAG, "Never!");
                }
                dialog.dismiss();
            }
        });
        ll.addView(b3);
        //ll.setRotation(270);
        dialog.setContentView(ll);
        
        dialog.show();        
    }
    
    public static void resetAllValues(Activity mContext){
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(mContext);
		final SharedPreferences.Editor editor = prefs.edit();
    	if(editor!=null){
    		editor.putBoolean(NOT_RATE_FOR_THIS_VERSION, false);
    		editor.putBoolean(NOT_RATE_FOR_ALL_VERSION, false);
    		editor.commit();
    	}
    	
    }
    
    private static boolean checkPlayStoreInstalled(Context context){
        try {
            context.getPackageManager().getPackageInfo(OldGooglePlayStorePackageName, 0);
        } catch (NameNotFoundException e) {
        	try{
        		context.getPackageManager().getPackageInfo(NewGooglePlayStorePackageName, 0);
        	} catch(NameNotFoundException e2){
        		return false;
        	}
        	return true;
            
        }
        return true;
    }
    
    private static boolean isGoogleChannel(Activity context){
    	Log.d(TAG,"Is Google Channel?");
    	try {
    		ApplicationInfo ai = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
			Bundle bundle = ai.metaData;
			String channel = bundle.getString("UMENG_CHANNEL");
			String googleChannel = context.getResources().getString(R.string.googleplay_channel);
			Log.d(TAG,"channel:"+channel+"   "+googleChannel);
			return channel.equals(googleChannel);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
    }

	public static boolean shouldRate( Activity mContext) {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(mContext);
		if(!isGoogleChannel(mContext)){
        	return false;
        }
    	
        if (prefs.getBoolean(NOT_RATE_FOR_ALL_VERSION, false)) {
        	Log.d(TAG, "Not rate for all");
        	return false; 
        	}
        if(!Locale.getDefault().getDisplayLanguage().equals(Locale.ENGLISH.getDisplayLanguage())&&!Locale.getDefault().getDisplayLanguage().equals(Locale.CHINESE.getDisplayLanguage())){
        	Log.d(TAG, "Not rate for non-English or Chinese");
        	return false;
        }
		
		int currentVersionCode = getVersionCode(mContext);
        int lastVersionCode = prefs.getInt(RATE_APP_VERSION_KEY, 0);
        if(lastVersionCode == currentVersionCode){
        	if(prefs.getBoolean(NOT_RATE_FOR_THIS_VERSION, false)){
        		Log.d(TAG, "Not rate for version:"+lastVersionCode+"  current version:"+currentVersionCode);
        		return false;
        	}
        }
	
		return true;
	}
	
	public static int getVersionCode(Activity mContext){
		PackageInfo pInfo;
		try {
			pInfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
			int currentVersionCode = pInfo.versionCode;
			return currentVersionCode;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
		
	}

	// see http://androidsnippets.com/prompt-engaged-users-to-rate-your-app-in-the-android-market-appirater
}
