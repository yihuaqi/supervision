package edu.harvard.schepens.supervisionv7;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.umeng.analytics.MobclickAgent;

public class UMENGhelper {
	static final String TAG = "UMENG_HELPER";
	static final boolean ENABLE_UMENG = true;
	
	/**
	 * No ongoing stabilization
	 */
	static final int STABILIZATION_STATUS_NONE = 0;
	/**
	 * All locked stabilization is ongoing.
	 */
	static final int STABILIZATION_STATUS_ALL_LOCKED = 1;
	/**
	 * Horizontally unlocked stabilization is ongoing.
	 */
	static final int STABILIZATION_STATUS_HORIZONTALLY_UNLOCKED = 2;
	/**
	 * Vertically unlocked stabilization is ongoing
	 */
	static final int STABILIZATION_STATUS_VERTICALLY_UNLOCKED = 3;
	/**
	 * Current stabilization status. Possible values:
	 * {@link #STABILIZATION_STATUS_ALL_LOCKED},
	 * {@link #STABILIZATION_STATUS_HORIZONTALLY_UNLOCKED},
	 * {@link #STABILIZATION_STATUS_NONE},
	 * {@link #STABILIZATION_STATUS_VERTICALLY_UNLOCKED};
	 */
	static int stabilization_status = STABILIZATION_STATUS_NONE;

	/**
	 * HashMap that stores a event start time.
	 */
	static HashMap<String, Long> eventTimeStamp = new HashMap<String, Long>();

	/**
	 * Event id of clicking FlashLight button that is used for reporting to
	 * Umeng support.
	 */
	public static final String EVENTID_FLASHLIGHT = "FlashLight";
	/**
	 * Event id of clicking InfoPage button that is used for report to Umeng
	 * support.
	 */
	public static final String EVENTID_INFOPAGE = "InfoPage";
	/**
	 * Event id of sharing a picture that is used for report to Umeng support.
	 */
	public static final String EVENTID_USERSHARE = "UserShare";
	/**
	 * Event id of clicking LockScreen button that is used for report to Umeng
	 * support.
	 */
	public static final String EVENTID_LOCKSCREEN = "LockScreen";
	/**
	 * Event id of touching screen to do stabilization that are used to report
	 * to Umeng support.
	 */
	public static final String EVENTID_STABILIZATION = "Stabilization";
	/**
	 * Event value of the all locked stabilization. This value is used when the
	 * stabilization starts and Horizontal lock is enabled.
	 */
	public static final String EVENTVALUE_ALL_LOCKED = "All locked";
	/**
	 * Event value of the horizontally unlocked. This value is used when the
	 * stabilization starts in portrait mode and Horizontal lock is disabled.
	 */
	public static final String EVENTVALUE_HORIZONTALLY_UNLOCKED = "Horizontally unlocked";
	/**
	 * Event value of the vertically unlocked. This value is used when the
	 * stabilization starts in landscape mode and Horizontal lock is disabled.
	 */
	public static final String EVENTVALUE_VERTICALLY_UNLOCKED = "Vertically unlocked";
	/**
	 * Currently not used.
	 */
	public static final String EVENTID_LAUNCHED = "Launched";
	/**
	 * Event value of user exit. The event happens when the Activity go to
	 * background, and reports the magnification.
	 */
	public static final String EVENTID_USEREXIT = "UserExit";
	/**
	 * Postfix of the event id if the user is a subject. If the user belongs to
	 * a group, then a postfix of group id should be added in addition to this
	 * postfix.
	 */

	public static final String EVENTID_USERFOCUS = "UserFocus";

	public static final String EVENTID_SP = "sp";
	/**
	 * Key for storing and getting the {@link #subjectId}.
	 */
	public static final String EVENTKEY_SUBJECT_ID = "Subject Id";
	/**
	 * Key for storing and getting the duration of the event.
	 */
	public static final String EVENTKEY_DURATION = "Duration";
	/**
	 * Key for storing and getting the event value.
	 */
	public static final String EVNETKEY_EVENTVALUE = "Event Value";
	/**
	 * Key for storing and getting the Stabilization Types. Possible values:
	 * {@link #EVENTVALUE_ALL_LOCKED}, {@link #EVENTVALUE_HORIZONTALLY_UNLOCKED}
	 * , {@link #EVENTVALUE_VERTICALLY_UNLOCKED}
	 */
	public static final String EVENTKEY_STABILIZATION_TYPE = "Stabilization Type";

	/**
	 * Given the type of the stabilization, records the start of this event. <br>
	 * Possible value: {@link Setting#EVENTVALUE_ALL_LOCKED},
	 * {@link Setting#EVENTVALUE_HORIZONTALLY_UNLOCKED},
	 * {@link Setting#EVENTVALUE_VERTICALLY_UNLOCKED};
	 * 
	 * @param context
	 * @param eventValue
	 */

	public static int REMAINING_CHECK_FOR_FOCUS = 3;
	public static float last_focus;

	public static void onStabilizationBegin(Context context, String eventValue) {
		if(ENABLE_UMENG){
			if (eventValue.equals(EVENTVALUE_ALL_LOCKED)) {
				stabilization_status = STABILIZATION_STATUS_ALL_LOCKED;
			} else if (eventValue.equals(EVENTVALUE_HORIZONTALLY_UNLOCKED)) {
				stabilization_status = STABILIZATION_STATUS_HORIZONTALLY_UNLOCKED;
			} else if (eventValue.equals(EVENTVALUE_VERTICALLY_UNLOCKED)) {
				stabilization_status = STABILIZATION_STATUS_VERTICALLY_UNLOCKED;
			}
			onEventBegin(context, EVENTID_STABILIZATION, eventValue);
			// MobclickAgent.onEventBegin(context, Setting.EVENTID_STABILIZATION,
			// eventValue);
		}
	}

	/**
	 * Reports a previous started Stabilization event.
	 * 
	 * @param context
	 */
	public static void onStabilizationEnd(Context context) {
		if(ENABLE_UMENG){
			HashMap<String, String> m = new HashMap<String, String>();
	
			switch (stabilization_status) {
			case STABILIZATION_STATUS_ALL_LOCKED:
				m.put(EVNETKEY_EVENTVALUE, EVENTVALUE_ALL_LOCKED);
				onEventEnd(context, EVENTID_STABILIZATION, m);
				// MobclickAgent.onEventEnd(context,
				// Setting.EVENTID_STABILIZATION,Setting.EVENTVALUE_ALL_LOCKED);
	
				break;
			case STABILIZATION_STATUS_HORIZONTALLY_UNLOCKED:
				m.put(EVNETKEY_EVENTVALUE, EVENTVALUE_HORIZONTALLY_UNLOCKED);
				onEventEnd(context, EVENTID_STABILIZATION, m);
	
				break;
			case STABILIZATION_STATUS_VERTICALLY_UNLOCKED:
				m.put(EVNETKEY_EVENTVALUE, EVENTVALUE_VERTICALLY_UNLOCKED);
				onEventEnd(context, EVENTID_STABILIZATION, m);
	
				break;
	
			default:
				break;
			}
			stabilization_status = STABILIZATION_STATUS_NONE;
		}
	}

	/**
	 * Report the start of lockscreen event.
	 * 
	 * @param context
	 */
	public static void onLockscreenBegin(Context context) {
		if(ENABLE_UMENG){
			onEventBegin(context, EVENTID_LOCKSCREEN);
		}

	}

	/**
	 * Reports the end of lockscreen event.
	 * 
	 * @param context
	 */
	public static void onLockscreenEnd(Context context) {
		if(ENABLE_UMENG){
			onEventEnd(context, EVENTID_LOCKSCREEN);
		}

	}

	/**
	 * Reports an infopage event.
	 * 
	 * @param context
	 */
	public static void onInfopage(Context context) {
		if(ENABLE_UMENG){
			onEvent(context, EVENTID_INFOPAGE);
		}
	}

	/**
	 * Reports an UserExit event.
	 * 
	 * @param context
	 * @param scale
	 */
	public static void onUserExit(Context context, float scale) {
		// HashMap<String, String> m = new HashMap<String, String>();

		// onEvent(context, Setting.EVENTID_USEREXIT,m);
		if(ENABLE_UMENG){
			if (scale > 0) {
				onEventValue(context, EVENTID_USEREXIT, null, (int) (scale * 1000));
			}
		}
	}

	/**
	 * Report a UserFocus Event.
	 * 
	 * @param context
	 * @param focus
	 *            focal distance of the camera.
	 */
	public static void onUserFocus(Context context, float focus) {
		if(ENABLE_UMENG){
			Log.d("AutoFocus", "" + focus);
			if (Setting.shouldSendFocus) {
				Log.d("AutoFocus", "Should SendFocus");
			}
			if (Setting.shouldSendFocus) {
				onEventValue(context, EVENTID_USERFOCUS, null, (int) (focus * 1000));
			} else {
				if (last_focus == 0.0) {
					last_focus = focus;
				} else {
					if (last_focus != focus) {
						Setting.shouldSendFocus = true;
					}
				}
			}
		}
	}

	/**
	 * Report a UserShare Event
	 * 
	 * @param context
	 */
	public static void onUserShare(Context context) {
		if(ENABLE_UMENG){
			onEvent(context, EVENTID_USERSHARE);
		}
	}

	/**
	 * Report an EventValue depending on whether the user is a subject and is in
	 * a group.
	 * 
	 * @param context
	 * @param eventid
	 * @param m
	 * @param valueOf
	 */
	private static void onEventValue(Context context, String eventid,
			Map<String, String> m, int valueOf) {
		if(ENABLE_UMENG){
			if (Setting.isSubject) {
				if (m == null) {
					m = new HashMap<String, String>();
				}
				String eventIdSp = eventid + EVENTID_SP;
				if (Setting.isInGroup) {
					eventIdSp += Setting.groupId;
				}
				m.put(EVENTKEY_SUBJECT_ID, Setting.subjectId);
				// onEventValue(context, eventid, m, valueOf);
				MobclickAgent.onEventValue(context, eventIdSp, m, valueOf);
			} else {
				MobclickAgent.onEventValue(context, eventid, m, valueOf);
			}
		}

	}

	/**
	 * Reports the start of a flash light event
	 * 
	 * @param context
	 */
	public static void onFlashLightBegin(Context context) {
		if(ENABLE_UMENG){
			onEventBegin(context, EVENTID_FLASHLIGHT);
		}
	}

	/**
	 * Reports the end of a flash light event
	 * 
	 * @param context
	 */
	public static void onFlashLightEnd(Context context) {
		if(ENABLE_UMENG){
			onEventEnd(context, EVENTID_FLASHLIGHT);
		}
	}

	/**
	 * Reports an EventBegin depending on whether the user is a subject or in a
	 * group.
	 * 
	 * @param context
	 * @param eventId
	 * @param eventValue
	 */
	private static void onEventBegin(Context context, String eventId,
			String eventValue) {
		if(ENABLE_UMENG){
			if (Setting.isSubject) {
				String eventIdSp = eventId + EVENTID_SP;
				if (Setting.isInGroup) {
					eventIdSp += Setting.groupId;
				}
				eventTimeStamp.put(eventIdSp, new Long(System.currentTimeMillis()));
	
			} else {
				MobclickAgent.onEventBegin(context, eventId, eventValue);
			}
		}
	}

	/**
	 * Reports an EventEnd depending on whether the user is a subject or in a
	 * group
	 * 
	 * @param context
	 * @param eventId
	 * @param eventValue
	 */
	private static void onEventEnd(Context context, String eventId,
			HashMap<String, String> m) {
		if(ENABLE_UMENG){
			if (Setting.isSubject) {
				String eventIdSp = eventId + EVENTID_SP;
				if (Setting.isInGroup) {
					eventIdSp += Setting.groupId;
				}
				Long eventStartTime = eventTimeStamp.get(eventIdSp);
	
				m.put(EVENTKEY_SUBJECT_ID, Setting.subjectId);
				if (eventStartTime != null) {
					// MobclickAgent.onEventDuration(context, eventIdSp,
					// m,System.currentTimeMillis() - eventStartTime.longValue());
					MobclickAgent.onEventValue(context, eventIdSp, m, (int) (System
							.currentTimeMillis() - eventStartTime.longValue()));
				}
			} else {
				MobclickAgent.onEventEnd(context, eventId,
						m.get(EVNETKEY_EVENTVALUE));
			}
		}

	}

	/**
	 * Reports an EventBegin depending on whether the user is a subject or in a
	 * group.
	 * 
	 * @param context
	 * @param eventId
	 */
	private static void onEventBegin(Context context, String eventId) {
		if(ENABLE_UMENG){
			if (Setting.isSubject) {
				String eventIdSp = eventId + EVENTID_SP;
				if (Setting.isInGroup) {
					eventIdSp += Setting.groupId;
				}
				eventTimeStamp.put(eventIdSp, new Long(System.currentTimeMillis()));
			} else {
				MobclickAgent.onEventBegin(context, eventId);
			}
		}
	}

	/**
	 * Reports an EventEnd depending on whether the user is a subject or in a
	 * group.
	 * 
	 * @param context
	 * @param eventId
	 */
	private static void onEventEnd(Context context, String eventId) {
		if(ENABLE_UMENG){
			if (Setting.isSubject) {
				String eventIdSp = eventId + EVENTID_SP;
				if (Setting.isInGroup) {
					eventIdSp += Setting.groupId;
				}
				Long eventStartTime = eventTimeStamp.get(eventIdSp);
				HashMap<String, String> m = new HashMap<String, String>();
				m.put(EVENTKEY_SUBJECT_ID, Setting.subjectId);
	
				if (eventStartTime != null) {
					// MobclickAgent.onEventDuration(context,
					// eventIdSp,System.currentTimeMillis() -
					// eventStartTime.longValue());
					MobclickAgent.onEventValue(context, eventIdSp, m, (int) (System
							.currentTimeMillis() - eventStartTime.longValue()));
				}
			} else {
				MobclickAgent.onEventEnd(context, eventId);
			}
		}
	}

	/**
	 * Reports an Event depending on whether the user is a subject or in a
	 * group.
	 * 
	 * @param context
	 * @param eventId
	 */
	private static void onEvent(Context context, String eventId) {
		if(ENABLE_UMENG){
			if (Setting.isSubject) {
				String eventIdSp = eventId + EVENTID_SP;
				if (Setting.isInGroup) {
					eventIdSp += Setting.groupId;
				}
				HashMap<String, String> m = new HashMap<String, String>();
				m.put(EVENTKEY_SUBJECT_ID, Setting.subjectId);
				MobclickAgent.onEvent(context, eventIdSp, m);
			} else {
				MobclickAgent.onEvent(context, eventId);
			}
		}
	}

	/**
	 * Reports an Event depending on whether the user is a subject or in a
	 * group.
	 * 
	 * @param context
	 * @param eventId
	 */
	private static void onEvent(Context context, String eventId,
			HashMap<String, String> m) {
		if(ENABLE_UMENG){
			if (Setting.isSubject) {
				String eventIdSp = eventId + EVENTID_SP;
				if (Setting.isInGroup) {
					eventIdSp += Setting.groupId;
				}
				m.put(EVENTKEY_SUBJECT_ID, Setting.subjectId);
				MobclickAgent.onEvent(context, eventIdSp, m);
			} else {
				MobclickAgent.onEvent(context, eventId, m);
			}
		}
	}

	public static void onCreate(Activity mainActivity) {
		if(ENABLE_UMENG){
			MobclickAgent.updateOnlineConfig(mainActivity);
			MobclickAgent.setDebugMode(true);
		}
	}

	public static void onPause(Activity mainActivity) {
		if(ENABLE_UMENG){
			MobclickAgent.onPause(mainActivity);
		}
		
	}

	public static void onResume(Activity mainActivity) {
		if(ENABLE_UMENG){
			MobclickAgent.onResume(mainActivity);
		}
		
	}

}
