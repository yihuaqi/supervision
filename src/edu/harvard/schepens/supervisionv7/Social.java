package edu.harvard.schepens.supervisionv7;

import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Button;

import com.umeng.socialize.bean.CustomPlatform;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.bean.SocializeEntity;
import com.umeng.socialize.controller.UMServiceFactory;
import com.umeng.socialize.controller.UMSocialService;
import com.umeng.socialize.controller.listener.SocializeListeners.OnSnsPlatformClickListener;
import com.umeng.socialize.controller.listener.SocializeListeners.SnsPostListener;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.sso.EmailHandler;
import com.umeng.socialize.sso.SmsHandler;
import com.umeng.socialize.sso.TencentWBSsoHandler;
import com.umeng.socialize.weixin.controller.UMWXHandler;

/**
 * 
 * This class handles all socialize operations, such as maintaining lifecycle,
 * handling click event.
 * 
 * @author Huaqi Yi
 * 
 */
public class Social {

	public static String FACEBOOK_APPID = "facebook id";

	/**
	 * Use the UMENG SDK for China.
	 */
	public static final int SOCIAL_UMENG_CHINA = 0;
	/**
	 * Use the UMENG SDK for US.
	 */
	public static final int SOCIAL_UMENG_US = 1;

	/**
	 * Determine which SDK we use for this app.
	 */
	public static int SOCIAL_TYPE = SOCIAL_UMENG_US;
	/**
	 * SocialAuth SDK adapter.
	 */

	public static boolean show_share_btn = true;

	/**
	 * UMENG SDK controller.
	 */
	private static UMSocialService mUMSocialController = null;
	private FragmentActivity mActivity;

	public Social(FragmentActivity a) {
		mActivity = a;
	}

	public void onResume(Activity context) {

		/*
		 * if(mFacebookHandler!=null){ mFacebookHandler.onResume(activity); }
		 */

	}

	public void onStop(Activity context) {
		/*
		 * if(mFacebookHandler!=null){ mFacebookHandler.onStop(); }
		 */

	}

	public void onSaveInstanceState(Activity context, Bundle outState) {
		/*
		 * if(mFacebookHandler!=null){
		 * mFacebookHandler.onSaveInstanceState(outState); }
		 */

	}

	public void onCreate(Activity context, Bundle savedInstanceState) {
		detectLanguage();

		switch (SOCIAL_TYPE) {

		case SOCIAL_UMENG_CHINA:
			Log.d("UMENG_CHINA", "OnCreate");
			mUMSocialController = UMServiceFactory
					.getUMSocialService("com.umeng.share");
			mUMSocialController.getConfig().setPlatforms(SHARE_MEDIA.SMS,
					SHARE_MEDIA.SINA, SHARE_MEDIA.EMAIL, SHARE_MEDIA.WEIXIN,
					SHARE_MEDIA.WEIXIN_CIRCLE, SHARE_MEDIA.TENCENT);
			mUMSocialController.getConfig().setSsoHandler(
					new TencentWBSsoHandler());

			String weixinAppId = context.getResources().getString(
					R.string.weixin_app_id);
			// Support Wechat
			UMWXHandler wxHandler = new UMWXHandler(context, weixinAppId);
			wxHandler.addToSocialSDK();

			// Support Wechat Circle
			UMWXHandler wxCircleHandler = new UMWXHandler(context, weixinAppId);
			wxCircleHandler.setToCircle(true);
			wxCircleHandler.addToSocialSDK();

			// Support Email
			EmailHandler emailHandler = new EmailHandler();
			emailHandler.addToSocialSDK();

			// Support SMS
			SmsHandler smsHandler = new SmsHandler();
			smsHandler.addToSocialSDK();
			break;
		case SOCIAL_UMENG_US:
			mUMSocialController = UMServiceFactory
					.getUMSocialService("com.umeng.share");
			mUMSocialController.getConfig().removePlatform(SHARE_MEDIA.DOUBAN);
			mUMSocialController.getConfig().removePlatform(SHARE_MEDIA.QQ);
			mUMSocialController.getConfig().removePlatform(SHARE_MEDIA.QZONE);
			mUMSocialController.getConfig().removePlatform(SHARE_MEDIA.WEIXIN);
			mUMSocialController.getConfig().removePlatform(
					SHARE_MEDIA.WEIXIN_CIRCLE);
			mUMSocialController.getConfig().removePlatform(SHARE_MEDIA.RENREN);
			mUMSocialController.getConfig().removePlatform(SHARE_MEDIA.SINA);
			mUMSocialController.getConfig().removePlatform(SHARE_MEDIA.TENCENT);
			mUMSocialController.getConfig().removePlatform(SHARE_MEDIA.YIXIN);
			mUMSocialController.getConfig().removePlatform(
					SHARE_MEDIA.YIXIN_CIRCLE);
			mUMSocialController.getConfig().removePlatform(SHARE_MEDIA.LAIWANG);
			mUMSocialController.getConfig().removePlatform(
					SHARE_MEDIA.LAIWANG_DYNAMIC);
			mUMSocialController.getConfig().removePlatform(SHARE_MEDIA.SMS);
			mUMSocialController.getConfig().removePlatform(SHARE_MEDIA.EMAIL);
			// Support SMS
			SmsHandler smsHandler2 = new SmsHandler();
			smsHandler2.addToSocialSDK();

			// Support Email
			EmailHandler emailHandler2 = new EmailHandler();
			emailHandler2.addToSocialSDK();

			// Customize the icon and caption of SMS.
			CustomPlatform customSMSPlatform = new CustomPlatform("SMS",
					R.drawable.umeng_socialize_sms);
			customSMSPlatform.mClickListener = new OnSnsPlatformClickListener() {

				@Override
				public void onClick(Context context, SocializeEntity entity,
						SnsPostListener listener) {
					mUMSocialController.shareSms(context);

				}
			};
			mUMSocialController.getConfig()
					.addCustomPlatform(customSMSPlatform);

			// Customize the icon and caption of Email.
			CustomPlatform customEMAILPlatform = new CustomPlatform("Email",
					R.drawable.umeng_socialize_gmail);
			customEMAILPlatform.mClickListener = new OnSnsPlatformClickListener() {

				@Override
				public void onClick(Context context, SocializeEntity entity,
						SnsPostListener listener) {
					mUMSocialController.shareEmail(context);

				}
			};
			mUMSocialController.getConfig().addCustomPlatform(
					customEMAILPlatform);

			break;

		default:
			break;
		}
	}

	/**
	 * Choose different social type based on the language.
	 */
	private void detectLanguage() {
		String lang = Locale.getDefault().getLanguage();
		if (lang.equals("zh")) {
			SOCIAL_TYPE = SOCIAL_UMENG_CHINA;
		} else {
			SOCIAL_TYPE = SOCIAL_UMENG_US;
		}

	}

	public void onStart(Activity context) {

	}

	public void onPause(Activity context) {

	}

	public void onDestroy(Activity context) {

	}

	/**
	 * Handle the click event on share button.
	 * 
	 * @param context
	 * @param bitmap
	 *            the image to be shared
	 */
	public void onClickShare(final Activity context, final Bitmap bitmap) {
		switch (SOCIAL_TYPE) {

		case SOCIAL_UMENG_CHINA:
			mUMSocialController.setShareImage(new UMImage(context, bitmap));
			mUMSocialController.openShare(context, new SnsPostListener() {

				@Override
				public void onStart() {
					Log.d("UM Share", "Share Start");

				}

				@Override
				public void onComplete(SHARE_MEDIA arg0, int arg1,
						SocializeEntity arg2) {
					Log.d("UM Share", arg0 + " " + arg1);
					if (arg1 == 200) {
						Log.d("UM Share", "Share Complete!");
						UMENGhelper.onUserShare(context);

					} else {
						Log.d("UM Share", "Share failed:" + arg1);
					}

				}
			});

			break;
		case SOCIAL_UMENG_US:
			mUMSocialController.setShareImage(new UMImage(context, bitmap));
			mUMSocialController.openShare(context, new SnsPostListener() {

				@Override
				public void onStart() {
					// TODO Auto-generated method stub

				}

				@Override
				public void onComplete(SHARE_MEDIA arg0, int arg1,
						SocializeEntity arg2) {
					Log.d("UM Share", arg0 + " " + arg1);
					if (arg1 == 200) {
						Log.d("UM Share", "Share Complete!");
						UMENGhelper.onUserShare(context);

					} else {
						Log.d("UM Share", "Share failed:" + arg1);
					}

				}
			});

			break;

		default:
			break;
		}

	}

	/**
	 * Init the shareButton for SocialAuth SDK.
	 * 
	 * @param mSharePictureBtn
	 */
	public void Init(Button mSharePictureBtn) {

	}

}
