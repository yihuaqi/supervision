package edu.harvard.schepens.supervisionv7;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.Format;
import java.text.NumberFormat;
import java.util.LinkedList;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.format.Time;
import android.util.Log;
import android.widget.Toast;

/**
 * This class handles saving snapshot picture, iterating through the gallery and
 * rotating picture for saving and displaying.
 * 
 * 
 * @author Huaqi Yi
 * 
 */
public class GalleryManager {
	private Context context;
	/**
	 * currentIndex >= -1 && currentIndex < mImageFiles.size(); -1 means the
	 * snapshot that is just taken.
	 * 
	 */
	private int currentIndex = 0;
	private Bitmap currentBitmap = null;
	

	
	/**
	 * This is all the files under SuperVision folder.
	 */
	private LinkedList<File> mImageFiles = new LinkedList<File>();

	private static GalleryManager manager = null;

	private GalleryManager() {

		// updateFiles();
	}

	public void setActivity(Context context) {
		this.context = context;
	}

	public static GalleryManager getInstance() {
		if (manager == null) {
			manager = new GalleryManager();
		}
		return manager;
	}

	/**
	 * Reads all image files under the folder.
	 */
	public void updateFiles() {
		if (getStorageDir().exists()) {
			mImageFiles.clear();
			File[] files = getStorageDir().listFiles();
			for (int i = 0; i < files.length; i++) {
				for (int j = 0; j <= i; j++) {
					if (j >= mImageFiles.size()) {
						mImageFiles.add(j, files[i]);
						break;
					} else {
						if (mImageFiles.get(j).lastModified() < files[i]
								.lastModified()) {
							mImageFiles.add(j, files[i]);
							break;
						}
					}
				}
			}
			
			for(int i = 0; i < mImageFiles.size(); i++){
				Log.d("GetBitmap","List:"+mImageFiles.get(i)+":"+i);
			}

		}
	}

	/**
	 * Get the storage folder.
	 * 
	 * @return
	 */
	private File getStorageDir() {
		return new File(
				Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				context.getResources().getString(R.string.app_name));
	}

	/**
	 * Rotates and saves the picture into gallery.
	 * 
	 * @param snapshot
	 * @param orientationMode
	 *            current orientation of the phone
	 */
	public void SavePicture() {

		Time time = new Time();
		time.setToNow();
		File storageDir = getStorageDir();
		if (!storageDir.exists()) {
			storageDir.mkdirs();
		}
		String newName = storageDir.getAbsolutePath() + "/"
				+ time.format2445() +".png";
		String fileName = storageDir.getAbsolutePath() + "/"
					+ "temporary" +".png";
		File temporary = new File(fileName);
		File saveAs = new File(newName);
		if(mImageFiles.size()>=1){
			mImageFiles.remove(0);
		}
		mImageFiles.addFirst(saveAs);
		if(temporary.renameTo(saveAs)){
			Toast.makeText(context, "Picture Saved", Toast.LENGTH_SHORT).show();
		}
		
		

	}

	/**
	 * AsyncTask that saves the picture into storage folder.
	 * 
	 * @author Huaqi Yi
	 * 
	 */
	private class SavePictureTask extends AsyncTask<Bitmap, Void, Void> {
		String fileName = "";
		Boolean savePermanent = false;
		public SavePictureTask(Boolean savePermanent){
			
			this.savePermanent = savePermanent;
			Time time = new Time();
			time.setToNow();
			File storageDir = getStorageDir();
			if (!storageDir.exists()) {
				storageDir.mkdirs();
			}
			if(savePermanent){
				fileName = storageDir.getAbsolutePath() + "/"
						+ time.format2445() +".png";
			} else {
				fileName = storageDir.getAbsolutePath() + "/"
						+ "temporary" +".png";
			}
		}
		
		@Override
		protected Void doInBackground(Bitmap... params) {

			Bitmap saveBitmap = params[0];

			if (saveBitmap == null) {
				Toast.makeText(context, "Image is empty", Toast.LENGTH_SHORT)
						.show();
				return null;
			}
			
			File snapshotFile = new File(fileName);

			// MediaStore.Images.Media.insertImage(getContentResolver(),mSnapShot,
			// "SuperVision"+time.format2445(),
			// "Snapshot taken from locking screen");
			try {
				//snapshotFile.createNewFile();
				FileOutputStream fos = new FileOutputStream(snapshotFile);
				saveBitmap.compress(CompressFormat.PNG, 100, fos);

				fos.close();
			} catch (Exception e) {

			}

			Intent mediaScanIntent = new Intent(
					Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);

			Uri contentUri = Uri.fromFile(snapshotFile);
			mediaScanIntent.setData(contentUri);
			context.sendBroadcast(mediaScanIntent);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			if(savePermanent){
				Toast.makeText(context,
						context.getResources().getString(R.string.picture_saved),
						Toast.LENGTH_SHORT).show();
			}

		}
	}
	
	

	/**
	 * Gets the previous image and rotates it based on the given orientation
	 * mode.
	 * 
	 * @param orientationMode
	 *            current orientation mode of the device
	 * @return A rotated bitmap of the previous saved image.
	 */
	public Bitmap getPrevBitmap(int orientationMode) {

		if (currentIndex + 1 < mImageFiles.size()) {
			currentIndex++;

			File bitmapFile = mImageFiles.get(currentIndex);
			Log.d("GetBitmap", mImageFiles.get(currentIndex)+":"+currentIndex);

			try {
				currentBitmap = null;
				currentBitmap = BitmapFactory.decodeStream(new FileInputStream(
						bitmapFile));
				if (currentBitmap != null) {
					return rotateImageToDisplay(currentBitmap, orientationMode);
				} else {
					throw new RuntimeException(
							"Cannot decode Previous Bitmap. File name:"
									+ bitmapFile.getName());
				}

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			Toast.makeText(context, "No more picture", Toast.LENGTH_SHORT).show();
		}

		return null;
	}

	/**
	 * Gets the next image and rotates it based on the given orientation mode.
	 * 
	 * @param orientationMode
	 *            current orientation mode of the device
	 * @return A rotated bitmap of the next saved image
	 */
	public Bitmap getNextBitmap(int orientationMode) {
		if (currentIndex > 0) {
			currentIndex--;

			File bitmaFile = mImageFiles.get(currentIndex);
			Log.d("GetBitmap", mImageFiles.get(currentIndex)+":"+currentIndex);
			try {
				currentBitmap = BitmapFactory.decodeStream(new FileInputStream(
						bitmaFile));
				return rotateImageToDisplay(currentBitmap, orientationMode);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			Toast.makeText(context, "No more picture", Toast.LENGTH_SHORT).show();
		}

		return null;

	}

	public Bitmap getCurrentBitmap() {

			return currentBitmap;
		
	}

	/**
	 * Resets the index to the current image.
	 */
	public void resetIndex() {
		currentIndex = 0;

	}

	/**
	 * Sets the snapshot bitmap for later display.
	 * 
	 * @param b
	 *            the underlying bitmap of the SnapshotView
	 */
	public void setSnapshotBitmap(Bitmap b, int orientationMode) {
		// mSnapshotBitmap = Bitmap.createBitmap(b);
		currentBitmap = b;
		
		SavePictureTask savePictureTask = new SavePictureTask(false);

		savePictureTask.execute(rotateImageToSave(b, orientationMode));
	}

	/**
	 * Rotates the image so that it is saved in right orientation.
	 * 
	 * @param b
	 *            bitmap to rotate
	 * @param mOrientationMode
	 *            current orientation mode of the device
	 * @return rotated bitmap
	 */
	public Bitmap rotateImageToSave(Bitmap b, int mOrientationMode) {
		Bitmap result = rotateImage(b, 90, mOrientationMode); 
		Log.d("OutOfMemoryLog", "---------------------After Rotation-------------------------");
		Log.d("OutOfMemoryLog", MemoryUsage.getMemoryUsage());
		Log.d("OutOfMemoryLog", "RotatedBitmap Size:"+(NumberFormat.getInstance().format(result.getByteCount()/1024)));
		return result;
	}

	/**
	 * Rotates the image so that it is displayed in right orientation.
	 * 
	 * @param b
	 *            bitmap to rotate
	 * @param mOrientationMode
	 *            current orientation mode of the device
	 * @return rotated bitmap
	 */
	public Bitmap rotateImageToDisplay(Bitmap b, int mOrientationMode) {
		Bitmap result = rotateImage(b, 270, mOrientationMode); 
		Log.d("OutOfMemoryLog", "---------------------After Rotation-------------------------");
		Log.d("OutOfMemoryLog", MemoryUsage.getMemoryUsage());
		Log.d("OutOfMemoryLog", "RotatedBitmap Size:"+(NumberFormat.getInstance().format(result.getByteCount()/1024)));
		return result;
	}

	/**
	 * Rotates the image based on the orientation mode and phase shift.
	 * 
	 * @param cachedBitmap
	 *            image to be rotated
	 * @param degree
	 *            phase shift
	 * @param mOrientationMode
	 *            orientation mode of the device
	 * @return
	 */
	private Bitmap rotateImage(Bitmap cachedBitmap, int degree,
			int mOrientationMode) {
		if (degree == 270) {
			Log.d("Rotate Image", "To display");
		} else if (degree == 90) {
			Log.d("Rotate Image", "To save");
		}
		Log.d("Rotate Image", "mode:" + mOrientationMode);

		Matrix rotateMatrix = new Matrix();
		switch (mOrientationMode) {
		case MainActivity.ORIENTATION_DOWN:
			rotateMatrix.postRotate((0 + degree) % 360);
			break;

		case MainActivity.ORIENTATION_LEFT:
			rotateMatrix.postRotate((270 + degree) % 360);
			break;
		case MainActivity.ORIENTATION_RIGHT:
			rotateMatrix.postRotate((90 + degree) % 360);
			break;
		case MainActivity.ORIENTATION_UP:
			rotateMatrix.postRotate((180 + degree) % 360);
			break;
		default:
			break;
		}
		Log.d("OutOfMemoryLog", "---------------------Before Rotation-------------------------");
		Log.d("OutOfMemoryLog", MemoryUsage.getMemoryUsage());
		Log.d("OutOfMemoryLog", "cacheBitmap Size:"+(NumberFormat.getInstance().format(cachedBitmap.getByteCount()/1024)));
		
		return Bitmap.createBitmap(cachedBitmap, 0, 0, cachedBitmap.getWidth(),
				cachedBitmap.getHeight(), rotateMatrix, false);
	}
}
