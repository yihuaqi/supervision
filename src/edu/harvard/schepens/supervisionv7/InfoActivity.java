package edu.harvard.schepens.supervisionv7;

import java.util.Locale;


import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;

/**
 * This activity is used for showing the info page.
 * @author zewenli
 *
 */
public class InfoActivity extends Activity{
	/**
	 * Back button for go back to main activity. Not used because android device has a back button.
	 */
	private Button mBackBtn;
	
	/**
	 * Webview that displays the info page.
	 */
	private WebView mInfoWebView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		

        setContentView(R.layout.info);
		mBackBtn = (Button) findViewById(R.id.info_back);
		mBackBtn.setOnClickListener(mOnClickListener);
        mInfoWebView = (WebView) findViewById(R.id.info_webview);
        if(Setting.ALLOW_INFO_REFLOW){

        	mInfoWebView.loadUrl("file:///android_res/raw/info_reflow.html");
	        mInfoWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
	        mInfoWebView.getSettings().setBuiltInZoomControls(true);
	        //mInfoWebView.getSettings().setUseWideViewPort(true);
	      //mInfoWebView.getSettings().setLayoutAlgorithm(LayoutAlgorithm.NORMAL);

        } else {
        	String lang = Locale.getDefault().getLanguage();
        	if(lang.equals("en")||lang.equals("ja")||lang.equals("zh")||lang.equals("ru")||lang.equals("es")||lang.equals("it")||lang.equals("fr")||lang.equals("de")){
        		lang = "_"+lang;
        	} else {
        		lang = "";
        	}
        	String filename = "file:///android_res/raw/info"+lang+".html";
	        mInfoWebView.loadUrl(filename);
	        
	        mInfoWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
	        mInfoWebView.getSettings().setBuiltInZoomControls(true);
	        mInfoWebView.getSettings().setUseWideViewPort(true);
        }
        

	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		UMENGhelper.onPause(this);
		

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		UMENGhelper.onResume(this);

	}

	private OnClickListener mOnClickListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			if(v==mBackBtn){
				finish();
			}
		}
	};
	
}
