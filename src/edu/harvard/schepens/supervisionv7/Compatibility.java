package edu.harvard.schepens.supervisionv7;

import java.util.Arrays;
import java.util.List;

import android.os.Build;

/**
 * This class is for solving compatibility issues.
 * 
 * @author Huaqi Yi
 * 
 */
public class Compatibility {
	/**
	 * 
	 * @return a String array of all S3 model name.
	 */
	private static String[] getS3ModelName() {
		String s3ModelNames[] = { "XXXXXXXXXXXXXXXX", // Place holder
				"SGH-I747", // AT&T
				"SGH-T999", // T-Mobile
				"SGH-N064", // Japan
				"SCH-R530", // US Cellular
				"SCH-I535", // Verizon
				"SPH-L710", // Sprint
				"GT-I9300", // International
				"SGH-I747", // AT&T
				"SGH-T999", // T-Mobile
				"SGH-N064", // Japan
				"SCH-R530", // US Cellular
				"SCH-I535", // Verizon
				"SPH-L710", // Sprint
				"GT-I9300" // International
		};
		return s3ModelNames;
	}

	/**
	 * 
	 * @return true if the phone is Galaxy SIII.
	 */
	public static boolean isGalaxyS3() {

		List<String> models = Arrays.asList(getS3ModelName());
		String model = Build.MODEL;

		return models.contains(model);

	}
}
